import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../services/configuration.service';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.css'] 
})
export class CloudComponent implements OnInit {
  title = 'app';
  cloudForm: FormGroup;
  submitted = false;  
  cloudFormData: any; 
  constructor(private formBuilder: FormBuilder, private configService: ConfigurationService) {
    this.createForm(); 
  }
  ngOnInit() {
    this.loadCloudConfig();
    
  }
  loadCloudConfig() {
    this.configService.getCloudConfig((response: any) => { 
      if (response.status && response.data && response.data.cloud_config) {
        this.cloudFormData = response.data.cloud_config;
        this.buildFormControls();
        this.cloudForm.patchValue(this.cloudFormData);      
      } else {
        alert("error occurred"+response.message);
      }
    }); 
   }


  buildFormControls() {
    this.cloudForm = this.formBuilder.group((
      {
        organizationName: [this.cloudFormData.organizationName, Validators.required],
        instanceUrl: [this.cloudFormData.instanceUrl, Validators.required],
        webServiceUrl: [this.cloudFormData.webServiceUrl, Validators.required],
        userName: [this.cloudFormData.userName, Validators.required],
        password: [this.cloudFormData.password, Validators.required]
      }
    ));
  }

  createForm() {
    this.cloudForm = this.formBuilder.group({
      organizationName: '',
      instanceUrl: '',
      webServiceUrl:'',
      userName:'',
      password:''
    });
  }
  

  get f() { return this.cloudForm.controls; }

  onSubmit(valuesObj) {
    console.log("cloud Submit::::", valuesObj, this.cloudFormData);
    this.submitted = true;
    // stop here if form is invalid
    if (this.cloudForm.invalid) {
      return;
    } 

    this.cloudFormData.organizationName= valuesObj.organizationName;
    this.cloudFormData.instanceUrl= valuesObj.instanceUrl;
    this.cloudFormData.webServiceUrl= valuesObj.webServiceUrl;
    this.cloudFormData.userName= valuesObj.userName;
    this.cloudFormData.password= valuesObj.password;
    
    this.configService.saveCloudConfig(this.cloudFormData)
      .subscribe(response => {
        if (response.status) { 
          this.loadCloudConfig();
           alert(response.message);
        } else {
          /*this.error = 'Username or password is incorrect';
            this.loading = false; */ 
            alert(response.message);
        }
      }, error => {
        console.log( error); 
      });

  }
}
