import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../services/configuration.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {
  rulesForm: FormGroup;
  rulesFormData: any;
  submitted = false;
  shiftPrioritizationData = [
    'FIFO',
    'FILO',
    'LIFO',
    'LILO'
  ];

  constructor(private formBuilder: FormBuilder, private configService: ConfigurationService) {
    this.createForm();
  }

  createForm() {
    this.rulesForm = this.formBuilder.group({
      removeDuplicates: [false],
      shifPriority: [''],
      shiftDifferences: ['']
    });
  }

  ngOnInit() {
    this.loadRulesConfig();
  }

  loadRulesConfig() {
    this.configService.getRulesConfig((response: any) => {
      if (response.status && response.data && response.data.rules_config) {
        this.rulesFormData = response.data.rules_config;
        this.buildFormControls();
        this.rulesForm.patchValue(response.data.rules_config);
      } else {
        alert("error occurred" + response.message);
      }
    });

  }

  buildFormControls() {
    this.rulesForm = this.formBuilder.group((
      {
        removeDuplicates: [this.rulesFormData.removeDuplicates],
        shifPriority: [this.rulesFormData.shifPriority, Validators.required],
        shiftDifferences: [this.rulesFormData.shiftDifferences, Validators.required] 
      }
    ));
  } 

  get f() { 
    return this.rulesForm.controls;
  }

  onSubmit(valuesObj) {
    console.log("Ruls Submit::::", valuesObj, this.rulesFormData);
    this.submitted = true;
    // stop here if form is invalid
    if (this.rulesForm.invalid) {
      return;
    }
    this.setFields(valuesObj);

    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.rulesForm.value))
    this.configService.saveRulesConfig(this.rulesFormData)
      .subscribe(response => {
        if (response.status) {
          this.loadRulesConfig();
          alert(response.message);
        } else {
          /*this.error = 'Username or password is incorrect';
            this.loading = false; */
          alert(response.message);
        }
      }, error => {
        console.log(error);
      });
  }

  setFields(valuesObj) {
    this.rulesFormData.removeDuplicates = valuesObj.removeDuplicates;
    this.rulesFormData.shifPriority = valuesObj.shifPriority;
    this.rulesFormData.shiftDifferences = valuesObj.shiftDifferences; 
  }    
}
