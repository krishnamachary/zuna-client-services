import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpHeaders, HttpResponseBase, HttpErrorResponse  } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'; 
@Injectable()
export class CommonService { 
  constructor(private http: HttpClient) {
  } 
  
  formatString(str) {
    if (str == null || str == "" || str == undefined) {
      return "";
    }
    return str;
  }
}