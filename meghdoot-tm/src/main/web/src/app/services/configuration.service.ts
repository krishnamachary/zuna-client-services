import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpHeaders, HttpResponseBase, HttpErrorResponse  } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LoginService } from '../login/login.service';

@Injectable()
export class ConfigurationService {

  authenticated = false;
  myHeaders: HttpHeaders = new HttpHeaders({'organizationId': this.loginService.organizationId,
                'Accept': 'application/json',  'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private loginService: LoginService) {
  } 

  getCloudConfig(callback): any {
    return this.http.get('configuration/getCloudConfigByOrganization?id='+this.loginService.organizationId,
    { headers: this.myHeaders})
    .subscribe((response: any )=> { 
            if (response.data != null) { 
                
            } else { 
            }
            return callback && callback(response);
     });
   }

  saveCloudConfig(config : any)  { 
    let headers = new HttpHeaders();
    headers = headers.append('organizationId', this.loginService.organizationId);
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');
        

    config['organization'] = {}
    config['organization'].id = this.loginService.organizationId; 
    return this.http.post("/configuration/saveCloudConfig",  config, { headers: headers })
    .map((response: any) =>  {
        console.log("cloud saving response:::::"+response);
         return response;
    }).catch((error:any) => Observable.throw(error || 'Server error'));
  }

  

  saveNotificationConfig(config : any)  { 
    
    config['organization'] = {}
    config['organization'].id = this.loginService.organizationId; 
    return this.http.post("/configuration/saveNotificationConfig",  config, { headers: this.myHeaders})
    .map((response: any) =>  {
        console.log("Notification saving response:::::"+response);
         return response;
    }).catch((error:any) => Observable.throw(error || 'Server error'));
  }

  getNotificationConfig(callback): any {
    return this.http.get('configuration/getNotificationConfigByOrganization?id='+this.loginService.organizationId,
    { headers: this.myHeaders})
    .subscribe((response: any )=> { 
            if (response.data != null) { 
                
            } else { 
            }
            return callback && callback(response);
     });
   }

   saveSchedulingConfig(config : any)  { 

    config['organization'] = {}
    config['organization'].id = this.loginService.organizationId; 
    return this.http.post("/configuration/savaScheduleConfig",  config, { headers: this.myHeaders})
    .map((response: any) =>  {
        console.log("Scheduling saving response:::::"+response);
         return response;
    }).catch((error:any) => Observable.throw(error || 'Server error'));
  }

  getSchedulingConfig(callback): any {
    return this.http.get('configuration/getScheduleConfigByOrganization?id='+this.loginService.organizationId,
    { headers: this.myHeaders})
    .subscribe((response: any )=> { 
            if (response.data != null) { 
                
            } else { 
            }
            return callback && callback(response);
     });
   }

   
   saveRulesConfig(config : any)  { 

    config['organization'] = {}
    config['organization'].id = this.loginService.organizationId; 
    return this.http.post("/configuration/saveRulesConfig",  config, { headers: this.myHeaders})
    .map((response: any) =>  {
        console.log("Rules saving response:::::"+response);
         return response;
    }).catch((error:any) => Observable.throw(error || 'Server error'));
  }

  getRulesConfig(callback): any {
    return this.http.get('configuration/getRulesConfigByOrganization?id='+this.loginService.organizationId,
    { headers: this.myHeaders})
    .subscribe((response: any )=> { 
            if (response.data != null) { 
                
            } else { 
            }
            return callback && callback(response);
     });
   }
   saveConfiguration(config, url): any {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('organizationId', this.loginService.organizationId);
        return this.http.post(url, config, { headers: headers })
        .map((response: any) =>  {
            console.log("response:::::"+response);
        }).catch((error:any) => Observable.throw(error || 'Server error'));
    }
    
    saveDatasource(config : any)  { 
      let headers = new HttpHeaders();
      headers = headers.append('organizationId', this.loginService.organizationId);
      headers = headers.append('Accept', 'application/json');
      headers = headers.append('Content-Type', 'application/json');
          
  
      config['organization'] = {}
      config['organization'].id = this.loginService.organizationId; 
      return this.http.post("/configuration/saveDatasource",  config, { headers: headers })
      .map((response: any) =>  {
          console.log("datsource saving response:::::"+response);
           return response;
      }).catch((error:any) => Observable.throw(error || 'Server error'));
    }

    getDatasource(callback): any {
      return this.http.get('configuration/getDatasourceByOrganization?id='+this.loginService.organizationId,
      { headers: this.myHeaders})
      .subscribe((response: any )=> { 
              if (response.data != null) { 
                  
              } else { 
              }
              return callback && callback(response);
       });
     }

}