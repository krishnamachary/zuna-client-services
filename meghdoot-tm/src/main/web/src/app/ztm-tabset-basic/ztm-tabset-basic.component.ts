import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ztm-tabset-basic',
  templateUrl: './ztm-tabset-basic.component.html',
  styles: [
    `
    :host { color: gray; }

    :host ::ng-deep .tabset1 a.nav-link {
      color: green;
    }
    
    :host ::ng-deep .tabset1 a.nav-link.active {
      color: red;
      font-weight: bold;
    }
`
  ]
})
export class ZtmTabsetBasicComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
