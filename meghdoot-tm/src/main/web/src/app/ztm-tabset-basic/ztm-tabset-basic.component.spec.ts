import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZtmTabsetBasicComponent } from './ztm-tabset-basic.component';

describe('ZtmTabsetBasicComponent', () => {
  let component: ZtmTabsetBasicComponent;
  let fixture: ComponentFixture<ZtmTabsetBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZtmTabsetBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZtmTabsetBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
