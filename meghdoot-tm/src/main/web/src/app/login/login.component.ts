import { Component, OnInit } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'] 
})
export class LoginComponent {

  credentials = {username: '', password: ''}; 
  loading = false;
  error = '';

  constructor(private app: LoginService, private http: HttpClient, private router: Router) {
  }

  login() {
   /*  this.app.authenticate(this.credentials, () => {
      if(this.app.authenticate) {
        this.router.navigateByUrl('/'); 
      } else {
        this.router.navigateByUrl('/login');; 
      }
    }); */
  
  this.app.authenticate(this.credentials)
  .subscribe(result => {
    if (result === true) { 
      this.router.navigateByUrl('/'); 
    } else {
        // login failed
        this.error = 'Username or password is incorrect';
        this.loading = false;
    } 
}, error => {
  this.loading = false;
  this.error = error;
}); 

   /*  this.app.authenticate(this.credentials);
    if(this.app.authenticate)
      this.router.navigate(['/cloud']);
    else
    this.router.navigate(['/']);
    return true; */
  }

}