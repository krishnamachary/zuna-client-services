import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpHeaders, HttpResponseBase, HttpErrorResponse  } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginService {

  authenticated = false;
  organizationId = '';

  constructor(private http: HttpClient) { 
    this.organizationId =  localStorage.getItem('organizationId');
  }

  /* authenticate(credentials, callback) {
    const headers = new HttpHeaders(credentials ? {
        authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password)
    } : {});

    this.http.get('setup/authenticate', {headers: headers}).subscribe(response => {
        if (response['name']) {
            this.authenticated = true;
        } else {
            this.authenticated = false;
        }
        return callback && callback();
    });
 }, */

   isUserLoogedIn(callback): any {
    return  this.http.get('setup/getUser').subscribe((response: any )=> {
        var userId = localStorage.getItem("userId");
            if (response.data != null && response.data.user && userId == response.data.user.userId) { 
                this.authenticated = true; 
            } else {
                this.authenticated = false;
            }
            return callback && callback(this.authenticated);
     });
   }

   authenticate(credentials): any {
        const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        let body = new HttpParams();
        body = body.set('username', credentials.username);
        body = body.set('password',  credentials.password); 
       return this.http.post('/setup/authenticate', body, { headers: myheader })//.subscribe(); 
        .map((response: any) =>  {
            // login successful if there's a jwt token in the response 
            if (response.status) { 
                this.organizationId = response.data.user.organization.id+"";
                 
                localStorage.setItem('currentUser', JSON.stringify(response.data.user)); 
                localStorage.setItem('userId', response.data.user.id); 
                localStorage.setItem('organizationId', response.data.user.organization.id+""); 
                // return true to indicate successful login
                this.authenticated = true;
                return true; 
            } else {
                // return false to indicate failed login
                this.authenticated = false;
                localStorage.removeItem('userId');
                localStorage.removeItem('currentUser');
                localStorage.removeItem('organizationId');
                this.organizationId = ''; 
                return false;
            } 
        }).catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    } 

  /*   authenticate(credentials, callback)  {
        const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        let body = new HttpParams();
        body = body.set('username', credentials.username);
        body = body.set('password',  credentials.password); 
        this.http.post('/setup/authenticate', body, { headers: myheader })  
        .subscribe( 
            (response: any )=>  {
            // login successful if there's a jwt token in the response 
                console.log(response.id);  
            if (response) {
                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify({response })); 
                localStorage.setItem('userId', response[0].id); 
                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            } 
        }, error => {
            console.log("error occurred  while authenticate:",error);
        });
    } */ 
}