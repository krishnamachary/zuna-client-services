import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from './login/login.service'; 
import 'rxjs/add/operator/finally';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { 
  
  constructor(private login: LoginService, private http: HttpClient, private router: Router) {
    var credentials = {};
     this.login.isUserLoogedIn((status: any) => {
        if (!status) {
          this.router.navigateByUrl('/login');
        }
    });  
  }
  logout() {
    this.http.post('logout', {}).finally(() => {
        this.login.authenticated = false;
        this.router.navigateByUrl('/login');
    }).subscribe();
  }

  isLoggedIn() {
    return this.login.authenticated;
  }

}
