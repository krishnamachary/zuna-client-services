import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  FormControl } from '@angular/forms';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { NotificationComponent } from './notification/notification.component';
import { CloudComponent } from './cloud/cloud.component';
import { SchedulingComponent } from './scheduling/scheduling.component';
import { DatasourceComponent } from './datasource/datasource.component';
import { RulesComponent } from './rules/rules.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { Injectable } from '@angular/core';
 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/* start of prime ng */
import {InputTextModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/calendar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {ToastModule} from 'primeng/toast';

import {MessageService} from 'primeng/api';

/* end of prime ng */

import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ConfigurationService } from './services/configuration.service';
import { CommonService } from './services/common.service';


@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const xhr = req.clone({
      headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
    });
    return next.handle(xhr);
  }
}

const appRoutes: Routes = [
  { path: '', redirectTo: 'cloud', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'datasource', component: DatasourceComponent },
  { path: 'notification', component: NotificationComponent },
  { path: 'cloud', component: CloudComponent },
  { path: 'scheduling', component: SchedulingComponent },
  { path: 'rules', component: RulesComponent }
//  { path: '**', redirectTo: 'login' } 
];
@NgModule({
  declarations: [
    AppComponent,
    NotificationComponent,
    CloudComponent,
    SchedulingComponent,
    DatasourceComponent,
    RulesComponent,
    LoginComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    CalendarModule,
    InputTextModule,
    RadioButtonModule,
    DropdownModule,
    ToastModule,
    BrowserAnimationsModule,
     HttpClientModule,RouterModule.forRoot(appRoutes)
  ],
  providers: [LoginService, { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true },
              ConfigurationService, CommonService, MessageService],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
