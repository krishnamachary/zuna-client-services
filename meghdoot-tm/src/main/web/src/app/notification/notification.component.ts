import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../services/configuration.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notificationForm: FormGroup;
  submitted = false;
  notificationFormData:any;

  constructor(private formBuilder: FormBuilder, private configService: ConfigurationService) {
    this.createForm();
  }
  

  ngOnInit() {
   this.loadNotificationConfig();
 }

 loadNotificationConfig() {
  this.configService.getNotificationConfig((response: any) => { 
    if (response.status && response.data && response.data.notification_config) {
      this.notificationFormData = response.data.notification_config;
      this.buildFormControls();
      this.notificationForm.patchValue(this.notificationFormData);      
    } else {
      alert("error occurred"+response.message);
    }
  }); 
 }

 buildFormControls() {
  this.notificationForm = this.formBuilder.group((
    {
      emailNotificationRequired: [this.notificationFormData.emailNotificationRequired],
      hostName: [this.notificationFormData.hostName, Validators.required],
      port: [this.notificationFormData.port, Validators.required],
      userName: [this.notificationFormData.userName, Validators.required],
      password: [this.notificationFormData.password, Validators.required],
      fromEmailId: [this.notificationFormData.fromEmailId, Validators.required],
      subject: [this.notificationFormData.fromEmailId, Validators.required],
      dailyNotification: [this.notificationFormData.fromEmailId],
      onErrorNotification:[this.notificationFormData.onErrorNotification]
    }
  ));
} 

createForm() {
    this.notificationForm = this.formBuilder.group({
      emailNotificationRequired: [''],
      hostName: [''],
      port: [''],
      userName: [''],
      password: [''], 
      fromEmailId: [''],
      subject: [''],
      dailyNotification : [''] ,
      onErrorNotification: ['']  
    });
  } 
 
  get f() { 
    return this.notificationForm.controls; 
  }
  
  onSubmit(valuesObj) {
    console.log("Notification Submit::::", valuesObj, this.notificationFormData);
    this.submitted = true; 
      
    // stop here if form is invalid
    if (this.notificationForm.invalid) {
      return;
    } 

    this.setFields(valuesObj);

    this.configService.saveNotificationConfig(this.notificationFormData)
    .subscribe(response => {
      if (response.status) {
        this.loadNotificationConfig();
         alert(response.message);
      } else {
        /*this.error = 'Username or password is incorrect';
          this.loading = false; */
          alert(response.message);
      }
    }, error => {
      console.log( error); 
    });
  }

  setFields(valuesObj) {
    this.notificationFormData.emailNotificationRequired = valuesObj.emailNotificationRequired;
    this.notificationFormData.hostName = valuesObj.hostName;
    this.notificationFormData.port = valuesObj.port;
    this.notificationFormData.userName = valuesObj.userName;
    this.notificationFormData.password = valuesObj.password;
    this.notificationFormData.fromEmailId = valuesObj.fromEmailId;
    this.notificationFormData.subject = valuesObj.subject;
    this.notificationFormData.dailyNotification = valuesObj.dailyNotification;
    this.notificationFormData.onErrorNotification = valuesObj.onErrorNotification; 
  }

}
