import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../services/configuration.service';
import { tap } from 'rxjs/operators';
import {SelectItem} from 'primeng/api';
import {DropdownModule} from 'primeng/dropdown';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-cloud',
  templateUrl: './datasource.component.html',
  styleUrls: ['./datasource.component.css'] 
})
export class DatasourceComponent implements OnInit {
  title = 'app';
  datasourceForm: FormGroup;
  submitted = false;  
  datasourceFormData: any; 
  datasources: SelectItem[];
  constructor(private formBuilder: FormBuilder, private configService: ConfigurationService, private messageService: MessageService) {
    this.createForm(); 
  }
  ngOnInit() {

    this.datasources = [
      {label:'-- Select --', value:null},
      {label:'My SQL', value:'mysql'},
      {label:'MS SQL', value:'MS SQL'},
      {label:'Postgre SQL', value:'Postgre SQL'},
      {label:'Oracle', value:'Oracle'}
  ];

    this.loadDatsource();
    
  }
  loadDatsource() {
    this.configService.getDatasource((response: any) => { 
      if (response.status && response.data && response.data.datasource) {
        this.datasourceFormData = response.data.datasource;
        this.buildFormControls();
        this.datasourceForm.patchValue(this.datasourceFormData);      
      } else {
        alert("error occurred"+response.message);
      }
    }); 
   }


  buildFormControls() {
    this.datasourceForm = this.formBuilder.group((
      {
        datasourceType: [this.datasourceFormData.datasourceType, Validators.required],
        hostName: [this.datasourceFormData.hostName, Validators.required],
        port: [this.datasourceFormData.port, Validators.required],
        schemaName: [this.datasourceFormData.schemaName, Validators.required],
        userName: [this.datasourceFormData.userName, Validators.required],
        password: [this.datasourceFormData.password, Validators.required]
      }
    ));
  }

  createForm() {
    this.datasourceForm = this.formBuilder.group({
      datasourceType: '',
      hostName: '',
      port:'',
      schemaName : '',
      userName:'',
      password:''
    });
  }
  

  get f() { return this.datasourceForm.controls; }

  onSubmit(valuesObj) {
    console.log("datasource Submit::::", valuesObj, this.datasourceFormData);
    this.submitted = true;
    // stop here if form is invalid
    if (this.datasourceForm.invalid) {
      return;
    } 

    this.datasourceFormData.datasourceType= valuesObj.datasourceType;
    this.datasourceFormData.hostName= valuesObj.hostName;
    this.datasourceFormData.port= valuesObj.port;
    this.datasourceFormData.schemaName= valuesObj.schemaName;
    this.datasourceFormData.userName= valuesObj.userName;
    this.datasourceFormData.password= valuesObj.password;
    
    this.configService.saveDatasource(this.datasourceFormData)
      .subscribe(response => {
        if (response.status) { 
          this.loadDatsource();
          this.messageService.add({severity:'success', summary:'Datasource Configuration Saved Sucessfully'});
        } else {
          /*this.error = 'Username or password is incorrect';
            this.loading = false; */ 
            this.messageService.add({severity:'error', summary:'Error occurred in Datasource Configuration'});
        }
      }, error => {
        console.log( error); 
      });

  }
}
