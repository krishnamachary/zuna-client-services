import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfigurationService } from '../services/configuration.service';
import { getLocaleDateFormat } from '@angular/common';
import { CommonService } from '../services/common.service';


@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.component.html',
  styleUrls: ['./scheduling.component.css']
})
export class SchedulingComponent implements OnInit {

  schedulingForm: FormGroup;
  schedulingFormData: any;
  submitted = false;
  public filteStartDateFrom: Date;
  public filterEndDateFrom: Date;

  frequencyData = [
    'Once',
    'Minutes/Hour',
    'Daily',
    'Weekly OR Bi-Weekly',
    'Monthly'
  ]
  constructor(private formBuilder: FormBuilder, private configService: ConfigurationService,
  private commonService:CommonService) {
    this.createForm();
  }

  createForm() {
    this.schedulingForm = this.formBuilder.group({ 
      filePath: ['', Validators.required],
      frequencyType: ['Once', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      everyInterval: [''],
      hourInterval:[''],
      minuteInterval:[''],
      monthlyDay:['']
    });
  }
 
  ngOnInit() {
    this.loadSchedulingConfig();
   
  }

  loadSchedulingConfig () {
    this.configService.getSchedulingConfig((response: any) => {
      if (response.status && response.data && response.data.schedule_config) {
        this.schedulingFormData = response.data.schedule_config; 
        if (this.schedulingFormData.id != null) {
          this.buildFormControls();
        this.schedulingForm.patchValue(this.schedulingFormData);
        }
      } else {
        alert("error occurred" + response.message);
      }
    });
  }

  buildFormControls() {
    this.schedulingFormData.startTime = this.getDate(this.schedulingFormData.startTime);
    this.schedulingFormData.endTime = this.getDate(this.schedulingFormData.endTime); 
    this.schedulingFormData.monthlyDay = this.commonService.formatString(this.schedulingFormData.monthlyDay);
    this.schedulingForm = this.formBuilder.group((
      {
        filePath: [this.schedulingFormData.filePath, Validators.required],
        frequencyType: [this.schedulingFormData.frequencyType, Validators.required],
        startTime: [this.schedulingFormData.startTime, Validators.required],
        endTime: [this.schedulingFormData.endTime, Validators.required],
        everyInterval: [this.schedulingFormData.everyInterval],
        hourInterval:[this.schedulingFormData.hourInterval],
        minuteInterval:[this.schedulingFormData.minuteInterval],
        monthlyDay:[this.schedulingFormData.monthlyDay]
      }
    ));
  }
  

  getDate(date) {
    if (date != null && date != "") {
      return new Date(date);
    }
    return "";
  }

  get f() {
    return this.schedulingForm.controls;
  }

  onSubmit(valuesObj) {
    console.log("scheduling Submit::::", valuesObj, this.schedulingFormData);
    this.submitted = true;
    // stop here if form is invalid
    if (this.schedulingForm.invalid) {
      this.findInvalidControls();
      return;
    } 
    this.setFields(valuesObj);

    this.configService.saveSchedulingConfig(this.schedulingFormData)
      .subscribe(response => {
        if (response.status) {
          this.loadSchedulingConfig();
          alert(response.message);
        } else {
          /*this.error = 'Username or password is incorrect';
            this.loading = false; */
          alert(response.message);
        }
      }, error => {
        console.log(error);
      });
  }
  public findInvalidControls() {
    const invalid = [];
    const controls = this.schedulingForm.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    console.log("invalids are:::"+invalid )
    return invalid;
}

  setFields(valuesObj) {
    this.schedulingFormData.filePath = valuesObj.filePath;
    this.schedulingFormData.frequencyType = valuesObj.frequencyType;
    this.schedulingFormData.startTime = valuesObj.startTime;
    this.schedulingFormData.endTime = valuesObj.endTime;
    this.schedulingFormData.everyInterval = valuesObj.everyInterval;
    this.schedulingFormData.hourInterval = valuesObj.hourInterval;
    this.schedulingFormData.minuteInterval = valuesObj.minuteInterval;
    this.schedulingFormData.monthlyDay = valuesObj.monthlyDay;
  }


}
