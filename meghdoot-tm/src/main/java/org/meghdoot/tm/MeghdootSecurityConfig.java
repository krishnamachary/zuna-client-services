package org.meghdoot.tm;

import javax.sql.DataSource;

import org.meghdoot.tm.service.impl.UserDetailsServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class MeghdootSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	DataSource dataSource;

	@Autowired
	private MeghdootAuthenticationEntryPoint authEntryPoint;
	
	@Autowired
	private UserDetailsServiceProvider userDetailsService;
	
	/*@Bean
    public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
    }*/
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(11);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("select user_name, password, enabled from users where user_name=?")
				.authoritiesByUsernameQuery("select user_name, authority from authorities where user_name=?")
				.passwordEncoder(new BCryptPasswordEncoder());*/
		
		//auth.authenticationProvider(authenticationProvider());
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	
/*	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
		entryPoint.setRealmName("Meghdoot");
		return entryPoint;
	}*/
	 

	@Override
	public void configure(WebSecurity web) throws Exception {
		//web.ignoring().antMatchers("/setup/**").antMatchers("/resources/**").antMatchers("/static/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		/* * http.csrf().disable();
		 * http.authorizeRequests().anyRequest().permitAll().and().httpBasic().
		 * authenticationEntryPoint(authenticationEntryPoint());
		*/ 
		//http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic();
		/*http.csrf().disable().authorizeRequests().anyRequest().hasAnyRole("ADMIN", "USER")
	    .and()
	    .httpBasic();*/
		
	/*  http.cors().and().authorizeRequests()
		.anyRequest().authenticated()
		.and()
		.formLogin().loginPage("/login").permitAll()
		.and()
		.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login");*/
		
		
		http.cors().and().authorizeRequests()
		 .antMatchers("/index.html", "/", "/home", "/setup/**",
			       "/login","/favicon.ico","/*.js","/*.js.map")
		//antMatchers("/login","/logout", "/static/**", "/static/assets/images/*.png")
		.permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login").permitAll()
		.and()
		.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
		.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
		.csrf().disable(); //csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());  

		
		 /*http.authorizeRequests()
	      .antMatchers("/index.html", "/", "/home", 
	       "/login","/favicon.ico","/*.js","/*.js.map").permitAll()
	      .anyRequest().authenticated().and().csrf().disable();*/
		
		/*http.cors().and()
		// starts authorizing configurations
		.authorizeRequests()
		// ignoring the guest's urls "
		.antMatchers("/login","/logout").permitAll()
		// authenticate all remaining URLS
		.anyRequest().fullyAuthenticated().and()
		.logout()
		.permitAll()
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "POST"))
		.and()
		// enabling the basic authentication
		.httpBasic().and()
		// configuring the session on the server
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
		// disabling the CSRF - Cross Site Request Forgery
		//.csrf().disable();
		.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());*/
	}
}