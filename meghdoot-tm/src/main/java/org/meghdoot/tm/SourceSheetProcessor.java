package org.meghdoot.tm;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.meghdoot.tm.custom.model.TimeSheetSource;
import org.meghdoot.tm.dto.TimeSheetSourceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class SourceSheetProcessor implements ItemProcessor<TimeSheetSourceDTO, TimeSheetSource> {

	private static final Logger log = LoggerFactory.getLogger(TimeSheetSource.class);

	@Override
   public TimeSheetSource process(final TimeSheetSourceDTO sourceDto) throws Exception {
        
       final TimeSheetSource timeSheetSource = new TimeSheetSource();
       timeSheetSource.setEmpId(sourceDto.getEmplId());
       timeSheetSource.setLegacyEmpId(sourceDto.getEmplId());
       timeSheetSource.setEmpName(sourceDto.getName());
       timeSheetSource.setAction(sourceDto.getAction().equalsIgnoreCase("IN")?1:0);
       timeSheetSource.setSourceType(sourceDto.getSourceType());
       timeSheetSource.setSourceId(sourceDto.getSourceId());
       Date dateTime = DateUtils.parseDate(sourceDto.getDate().concat(" ").concat(sourceDto.getTime()), new String[] {"MM/dd/yy HH:mm:ss"});
       dateTime.setYear(dateTime.getYear());
       timeSheetSource.setDate(dateTime);
       log.info("Converting (" + sourceDto + ") into (" + timeSheetSource + ")");
       return timeSheetSource;
   }
	
}