package org.meghdoot.tm;

import javax.sql.DataSource;

import org.meghdoot.tm.custom.model.TimeSheetSource;
import org.meghdoot.tm.dto.TimeSheetSourceDTO;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class TimeSheetSourceBatching {
	
	@Autowired StepBuilderFactory stepBuilderFactory;
	
	@Autowired JobBuilderFactory jobBuilderFactory;
	
	@Autowired DataSource dataSource;
	
	@Bean
	 public FlatFileItemReader<TimeSheetSourceDTO> csvAnimeReader(){
	     FlatFileItemReader<TimeSheetSourceDTO> reader = new FlatFileItemReader<TimeSheetSourceDTO>();
	     reader.setResource(new ClassPathResource("source.csv"));
	     reader.setLinesToSkip(1);
		 reader.setLineMapper(new DefaultLineMapper<TimeSheetSourceDTO>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer(",") {
					{
						// setNames(new String[] { "EmpId", "SourceType", "source", "Action", "Date",
						// "Time", "Name" });
						setNames(new String[] { "emplId", "sourceType", "sourceId", "action", "date", "time", "name" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<TimeSheetSourceDTO>() {
					{
						setTargetType(TimeSheetSourceDTO.class);
					}
				});
	     }});
	     return reader;
	 }
	
	@Bean
	ItemProcessor<TimeSheetSourceDTO, TimeSheetSource> csvAnimeProcessor() {
	    return new SourceSheetProcessor();
	}
	
	@Bean
	public JdbcBatchItemWriter<TimeSheetSource> csvAnimeWriter() {
	     JdbcBatchItemWriter<TimeSheetSource> excelAnimeWriter = new JdbcBatchItemWriter<TimeSheetSource>();
	     excelAnimeWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<TimeSheetSource>());
	     excelAnimeWriter.setSql("INSERT INTO time_details_interface (EMPLID, LEGACY_EMPLID, NAME, EVENTTYPE, EVENTDATETIME, SOURCETYPE, SOURCEID, SOURCENAME) VALUES (:empId, :legacyEmpId, :empName, :action, :date, :sourceType, :sourceId, :sourceName)");
	     excelAnimeWriter.setDataSource(dataSource);
	        return excelAnimeWriter;
	}
	
	@Bean
	public Step csvFileToDatabaseStep() {
	    return stepBuilderFactory.get("timeSheetSourceJob")
	            .<TimeSheetSourceDTO, TimeSheetSource>chunk(1)
	            .reader(csvAnimeReader())
	            .processor(csvAnimeProcessor())
	            .writer(csvAnimeWriter())
	            .build();
	}
	
	@Bean
	Job csvFileToDatabaseJob() {
	    return jobBuilderFactory.get("timeSheetSourceJob")
	            .incrementer(new RunIdIncrementer())
	            .flow(csvFileToDatabaseStep())
	            .end()
	            .build();
	}

}
