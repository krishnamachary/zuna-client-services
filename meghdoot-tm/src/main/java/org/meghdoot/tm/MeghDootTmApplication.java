package org.meghdoot.tm;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages={"org.meghdoot.tm"})
@EnableIntegration
@EnableScheduling
@ImportResource({"classpath:tm-integration.xml"})
@EnableBatchProcessing
//TODO Remove org.meghdoot.tm.custom.repository. Need to work as jdbc template
@EnableJpaRepositories({"org.meghdoot.tm.repository", "org.meghdoot.tm.custom.repository"})
@EnableTransactionManagement
public class MeghDootTmApplication {

	public static void main(String[] args) {
		System.setProperty("jasypt.encryptor.password", "m3g#do0T");
		SpringApplication.run(MeghDootTmApplication.class, args);
	}
	
}
