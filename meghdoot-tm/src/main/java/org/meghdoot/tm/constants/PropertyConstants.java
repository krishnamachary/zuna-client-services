package org.meghdoot.tm.constants;

public class PropertyConstants {
	public static final String SCHEDULE = "schedule";
	public static final String ONCE = "ONCE";
	public static final String MINUTES_HOURS = "Minutes/Hours";
	public static final String DAILY = "DAILY";
	public static final String WEEK_BI_WEEKLY = "Weekly OR Bi-Weekly";
	public static final String MONTHLY = "Monthly";
	public static final String FIFO = "FIFO";
	public static final String LIFO = "LIFO";
	public static final String FILO = "FILO";
	public static final String LILO = "LILO";
	public static final String CLOUD_CONFIG = "cloud_config";
	public static final String NOTIFICATION_CONFIG = "notification_config";
	public static final String RULES_CONFIG = "rules_config";
	public static final String SCHEDULE_CONFIG = "schedule_config";
	public static final String DATASOURCE = "datasource";
	public static final String USER_SESSION = "userSession";
	public static final String USER = "user";
	
	
}
