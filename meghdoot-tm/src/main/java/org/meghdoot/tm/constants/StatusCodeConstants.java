package org.meghdoot.tm.constants;

public class StatusCodeConstants {
	public static final String INVALID_EMAIL = "invalidEmail";
	public static final String SUBJECT_IS_REQUIRED = "subject_is_required";
	public static final String FILE_PATH_IS_REQUIRED = "file_path_is_required";
	public static final String ORG_NAME_IS_REQUIRED = "org_name_is_required";
	public static final String ORGANIZATION_IS_REQUIRED = "organization_is_required";
	public static final String SOMETHING_WENT_WRONG_TRY_AFTER_SOMETIME = "something_went_wrong_try_after_sometime";
	public static final String CLOUD_CONFIG_SAVED = "cloud_config_saved";
	public static final String DATA_SOURCE_SAVED = "data_source_saved";
	public static final String NOTIFICATION_CONFIG_SAVED = "notification_config_saved";
	public static final String SCHEDULE_CONFIG_SAVED = "schedule_config_saved";
	public static final String RULES_CONFIG_SAVED = "rules_config_saved";
	public static final String REQUEST_SUCCESSFULL = "request_successfull";
	public static final String USER_LOGIN_SUCCESS = "user_login_success";
	public static final String INVALID_CREDENTIALS = "invalid_credentials";
	public static final String USER_NOT_FOUND = "user_not_found";
	public static final String NO_CONFIGURATION_FOUND = "no_configuration_found";
}
