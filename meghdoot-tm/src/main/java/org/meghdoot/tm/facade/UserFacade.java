package org.meghdoot.tm.facade;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.meghdoot.tm.constants.PropertyConstants;
import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.dto.AuthoritiesDTO;
import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.dto.UserDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.exception.NoSuchUserException;
import org.meghdoot.tm.model.Authorities;
import org.meghdoot.tm.service.UserService;
import org.meghdoot.tm.session.UserPrincipal;
import org.meghdoot.tm.util.ApplicationUtil;
import org.meghdoot.tm.util.DateUtil;
import org.meghdoot.tm.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service("userFacade")
public class UserFacade {
	private static final Logger LOG = LoggerFactory.getLogger(UserFacade.class);

	@Autowired
	private UserService userService;
	
	public Response authenticate(String userName, String password) {
		Response response = new Response();
		try {
			response = loginUser(userName, password);
			if (response.getStatus()) {
				authenticateUser(userName);
			}
		} catch (Exception e) {
			LOG.info("Exception in authenticate");
			LOG.error("Exception in authenticate", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}

	public Response loginUser(String userName, String password) {
		Response resp = new Response();
		try {
			UserDTO userDto = userService.loginUser(userName, password);
			if (userDto != null && userDto.getEnabled()) {
				resp.setStatus(Boolean.TRUE);
				resp.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.USER_LOGIN_SUCCESS, null));
				UserVO user = new UserVO();
				BeanUtils.copyProperties(userDto, user);
				resp.setProperty(PropertyConstants.USER, user);
			} else {
				resp.setStatus(Boolean.FALSE);
				resp.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.INVALID_CREDENTIALS, null));
			}
		} catch (NoSuchUserException e) {
			resp.setStatus(Boolean.FALSE);
			resp.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.USER_NOT_FOUND, null));
		} catch (AppException e) {e.printStackTrace();
			resp.setStatus(Boolean.FALSE);
			resp.setMessage(
					ApplicationUtil.getMessage(StatusCodeConstants.SOMETHING_WENT_WRONG_TRY_AFTER_SOMETIME, null));
		}
		return resp;
	}

	public void authenticateUser(String userName) throws AppException {
		try {
			UserDTO user = userService.findByUserName(userName);
			Set<GrantedAuthority> authorities = new HashSet<>();
			List<AuthoritiesDTO> roles = user.getAuthorities();
			for (AuthoritiesDTO role : roles) {
				authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
			}
			UserDetails userDetails = new UserPrincipal(userName, user.getPassword(), user.getEnabled(), true,
					true, true, authorities, user.getId(), user.getFirstName(), user.getLastName(), user.getEmailId(),
					DateUtil.getDateFormat(user.getLastLoginDate(), null), user.getEmailId());

			Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, null,
					userDetails.getAuthorities());

			SecurityContextHolder.getContext().setAuthentication(auth);
		} catch (Exception e) {
			LOG.info("Exception in authenticateUser: " + userName);
			LOG.error("Exception in authenticateUser: " + userName, e);
		}
	}

}
