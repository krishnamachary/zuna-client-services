package org.meghdoot.tm.facade;

import org.meghdoot.tm.TmContextLocal;
import org.meghdoot.tm.constants.PropertyConstants;
import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.dto.CloudDTO;
import org.meghdoot.tm.dto.DatasourceDTO;
import org.meghdoot.tm.dto.NotificationDTO;
import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.dto.RulesDTO;
import org.meghdoot.tm.dto.ScheduleDTO;
import org.meghdoot.tm.model.DataSource;
import org.meghdoot.tm.service.CloudService;
import org.meghdoot.tm.service.DataSourceService;
import org.meghdoot.tm.service.NotificationService;
import org.meghdoot.tm.service.RulesService;
import org.meghdoot.tm.service.ScheduleService;
import org.meghdoot.tm.util.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("configurationFacade")
public class ConfigurationFacade {
	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationFacade.class);
	
	@Autowired
	private CloudService cloudService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private DataSourceService dataSourceService;
	@Autowired
	private RulesService rulesService;
	
	public Response saveCloudConfig(CloudDTO cloudConfig) {
		Response response = new Response();
		try {
			OrganizationDTO org = new OrganizationDTO();
			org.setId(cloudConfig.getOrganization().getId()); 
			cloudConfig.setOrganization(org);
			cloudService.persistCloudConfig(cloudConfig);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.CLOUD_CONFIG_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in saveCloudConfig");
			LOG.error("Exception in saveCloudConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response savaDatasource(DataSource dataSource) {
		Response response = new Response();
		try {
			dataSource.setOrganization(TmContextLocal.get().getOrganization());
			dataSource.setJdbcUrl("jdbc:"+dataSource.getDatasourceType()+"://"+dataSource.getHostName()+":"+dataSource.getPort()+"/"+dataSource.getSchemaName());
			dataSourceService.persistDataSource(dataSource); 
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.CLOUD_CONFIG_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in savaCloudConfig");
			LOG.error("Exception in savaCloudConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response savaNotificationConfig(NotificationDTO notificationDTO) {
		Response response = new Response();
		try {
			OrganizationDTO org = new OrganizationDTO();
			org.setId(notificationDTO.getOrganization().getId());  
			notificationDTO.setOrganization(org);
			notificationService.persistNotificationConfig(notificationDTO);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.NOTIFICATION_CONFIG_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in savaNotificationConfig");
			LOG.error("Exception in savaNotificationConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response saveScheduleConfig(ScheduleDTO scheduleDTO) {
		Response response = new Response();
		try {
			OrganizationDTO org = new OrganizationDTO();
			org.setId(scheduleDTO.getOrganization().getId());
			scheduleDTO.setOrganization(org);
			scheduleService.persistScheduleConfig(scheduleDTO);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.SCHEDULE_CONFIG_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in savaScheduleConfig");
			LOG.error("Exception in savaScheduleConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response savaRulesConfig(RulesDTO rulesDTO) {
		Response response = new Response();
		try {
			OrganizationDTO org = new OrganizationDTO();
			org.setId(rulesDTO.getOrganization().getId()); 
			rulesDTO.setOrganization(org);
			rulesService.persistRulesConfig(rulesDTO);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.RULES_CONFIG_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in savaRulesConfig");
			LOG.error("Exception in savaRulesConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response savaDatsource(DataSource dataSource) {
		Response response = new Response();
		try {
			dataSource.setOrganization(TmContextLocal.get().getOrganization());
			dataSourceService.persistDataSource(dataSource);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.DATA_SOURCE_SAVED, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.error("Exception in savaRulesConfig", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response getCloudConfigByOrganization(String id) {
		Response response = new Response();
		try {
			CloudDTO cloud = cloudService.getCloudConfigByOrganization(id);
			response.setProperty(PropertyConstants.CLOUD_CONFIG, cloud);
			response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.REQUEST_SUCCESSFULL, null));
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in getCloudConfigByOrganization");
			LOG.error("Exception in getCloudConfigByOrganization", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response getNotificationConfigByOrganization(String id) {
		Response response = new Response();
		try {
			NotificationDTO notification = notificationService.getNotificationConfigByOrganization(id);
			if (notification != null) {
				response.setProperty(PropertyConstants.NOTIFICATION_CONFIG, notification);
			} else {
				response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.NO_CONFIGURATION_FOUND, null));
			}
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in getNotificationConfigByOrganization");
			LOG.error("Exception in getNotificationConfigByOrganization", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response getRulesConfigByOrganization(String id) {
		Response response = new Response();
		try {
			response.setStatus(Boolean.TRUE);
			RulesDTO rules = rulesService.getRulesConfigByOrganization(id);
			if (rules != null) {
				response.setProperty(PropertyConstants.RULES_CONFIG, rules);
			} else {
				response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.REQUEST_SUCCESSFULL, null));
			}
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in getRulesConfigByOrganization");
			LOG.error("Exception in getRulesConfigByOrganization", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response getScheduleConfigByOrganization(String id) {
		Response response = new Response();
		try {
			ScheduleDTO schedule = scheduleService.getScheduleConfigByOrganization(id);
			if (schedule != null) {
				response.setProperty(PropertyConstants.SCHEDULE_CONFIG, schedule);
			} else {
				response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.NO_CONFIGURATION_FOUND, null));
			}
			response.setStatus(Boolean.TRUE);
		} catch (Exception e) {
			LOG.info("Exception in getScheduleConfigByOrganization");
			LOG.error("Exception in getScheduleConfigByOrganization", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
	public Response getDatasourceByOrganization(String id) {
		Response response = new Response();
		try {
			DatasourceDTO datasource = dataSourceService.getDatasourceByOrganization(Long.valueOf(id));
			if (datasource != null) {
				response.setProperty(PropertyConstants.DATASOURCE, datasource);
				response.setStatus(Boolean.TRUE);
			} else {
				response.setMessage(ApplicationUtil.getMessage(StatusCodeConstants.NO_CONFIGURATION_FOUND, null));
			}
			
		} catch (Exception e) {
			LOG.error("Exception in getDatasourceByOrganization", e);
			response = ApplicationUtil.getExceptionBaseResponse(e.getMessage());
		}
		return response;
	}
	
}
