package org.meghdoot.tm.custom.repository;

import java.util.List;

import org.meghdoot.tm.custom.model.TimeSheetSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeSheetSourceRepository extends JpaRepository<TimeSheetSource, Long> {

	@Query(nativeQuery=true, value="SELECT DISTINCT EMPLID FROM time_details_interface")
	List<Long> getAllEmployeeIds();
	
	@Query(value="SELECT ts FROM TimeSheetSource ts where ts.requestStatus = ?1")
	List<TimeSheetSource> getAllSources(String status);
}
