package org.meghdoot.tm.custom.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "time_card_details")
@Entity
public class TimeCardDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8194426011312585215L;
	@Id
	@Column(name = "REQUESTID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long requestId;
	
	@Column(name = "EMPLID")
	private String empId;
	
	@Column(name = "LEGACY_EMPLID")
	private String legacyEmpId;
	
	@Column(name = "SOURCENAME")
	private String sourceName;
	
	@Column(name = "SOURCETYPE")
	private String sourceType;
	
	@Column(name = "EVENTTYPE")
	private Integer action;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EVENTDATETIME")
	private Date date;
	
	@Column(name = "REQUEST_STATUS")
	private String requestStatus;
	
	@Column(name = "MAIL_NOTIFYFLAG")
	private String mailNotify;
	
	@Column(name = "DOWNTIME_FLAG")
	private String downTimeNotify;
	
	@Column(name = "NETWORK_ERRFLAG")
	private String networkError;
	
	@Column(name = "ERRORCODE")
	private String errCode;
	
	@Column(name = "ERRORMSG")
	private String errMsg;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DTTM")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DTTM")
	private Date updatedDate;

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getLegacyEmpId() {
		return legacyEmpId;
	}

	public void setLegacyEmpId(String legacyEmpId) {
		this.legacyEmpId = legacyEmpId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public Integer getAction() {
		return action;
	}

	public void setAction(Integer action) {
		this.action = action;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getMailNotify() {
		return mailNotify;
	}

	public void setMailNotify(String mailNotify) {
		this.mailNotify = mailNotify;
	}

	public String getDownTimeNotify() {
		return downTimeNotify;
	}

	public void setDownTimeNotify(String downTimeNotify) {
		this.downTimeNotify = downTimeNotify;
	}

	public String getNetworkError() {
		return networkError;
	}

	public void setNetworkError(String networkError) {
		this.networkError = networkError;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}

}
