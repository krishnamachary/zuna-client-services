package org.meghdoot.tm.custom.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "time_details_interface")
@Entity
public class TimeSheetSource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3307994667058839091L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "EMPLID")
	private String empId;
	
	@Column(name = "LEGACY_EMPLID")
	private String legacyEmpId;
	
	@Column(name = "SOURCETYPE")
	private String sourceType;
	
	@Column(name = "SOURCEID")
	private String sourceId;
	
	@Column(name = "SOURCENAME")
	private String sourceName;
	
	@Column(name = "EVENTTYPE")
	private Integer action;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EVENTDATETIME")
	private Date date;
	
	@Column(name = "NAME")
	private String empName;
	
	@Column(name = "REQUESTSTATUS")
	private String requestStatus;
	
	@Column(name = "ERRORMSG")
	private String errMsg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getLegacyEmpId() {
		return legacyEmpId;
	}

	public void setLegacyEmpId(String legacyEmpId) {
		this.legacyEmpId = legacyEmpId;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getAction() {
		return action;
	}

	public void setAction(Integer action) {
		this.action = action;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getDate() {
		return date;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	public String getSourceName() {
		return sourceName;
	}

	@Override
	public String toString() {
		return "TimeSheetSource [id=" + id + ", empId=" + empId + ", legacyEmpId=" + legacyEmpId + ", sourceType="
				+ sourceType + ", sourceId=" + sourceId + ", action=" + action + ", date=" + date + ", empName="
				+ empName + ", requestStatus=" + requestStatus + ", errMsg=" + errMsg + "]";
	}

}
