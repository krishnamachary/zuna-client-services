package org.meghdoot.tm.custom.repository;

import org.meghdoot.tm.custom.model.TimeCardDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeCardDetailsRepository extends JpaRepository<TimeCardDetails, Long>  {

}
