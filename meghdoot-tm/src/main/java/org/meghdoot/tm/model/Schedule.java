package org.meghdoot.tm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.Audit.Auditable;
import org.meghdoot.tm.constants.PropertyConstants;
import org.meghdoot.tm.constants.StatusCodeConstants;

@Entity
@Table(name = PropertyConstants.SCHEDULE)
public class Schedule extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = -8436228087698366242L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = StatusCodeConstants.FILE_PATH_IS_REQUIRED)
	private String filePath;
	private String frequencyType;
	private String cronExpression;
	@ElementCollection
	private List<String> frequencySubTypes;
	private Date startTime;
	private Date endTime;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	@OneToOne
	@JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "FK_schedule_organization"))
	private Organization organization;
	private String everyInterval;
	private String hourInterval;
	private String minuteInterval;
	private String monthlyDay;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFrequencyType() {
		return frequencyType;
	}

	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public List<String> getFrequencySubTypes() {
		return frequencySubTypes;
	}

	public void setFrequencySubTypes(List<String> frequencySubTypes) {
		this.frequencySubTypes = frequencySubTypes;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getEveryInterval() {
		return everyInterval;
	}

	public void setEveryInterval(String everyInterval) {
		this.everyInterval = everyInterval;
	}

	public String getMinuteInterval() {
		return minuteInterval;
	}

	public void setMinuteInterval(String minuteInterval) {
		this.minuteInterval = minuteInterval;
	}

	public String getHourInterval() {
		return hourInterval;
	}

	public void setHourInterval(String hourInterval) {
		this.hourInterval = hourInterval;
	}

	public String getMonthlyDay() {
		return monthlyDay;
	}

	public void setMonthlyDay(String monthlyDay) {
		this.monthlyDay = monthlyDay;
	}

}
