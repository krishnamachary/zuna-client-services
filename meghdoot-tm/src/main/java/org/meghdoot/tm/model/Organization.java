package org.meghdoot.tm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.meghdoot.tm.Audit.Auditable;
import org.meghdoot.tm.constants.StatusCodeConstants;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "organization")
@JsonIgnoreProperties(value = { "createdAt", "modifiedAt" }, allowGetters = true)
public class Organization extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1755081663097746827L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="organization_id")
	private Long id;
	
	@Column(name="organization_name")
	@NotBlank(message = StatusCodeConstants.ORG_NAME_IS_REQUIRED)
	@JsonAlias("organizationName")
	private String name;

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
