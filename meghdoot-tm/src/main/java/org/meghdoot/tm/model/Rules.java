package org.meghdoot.tm.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.Audit.Auditable;
import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.enums.ShifPriority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "rules")
@JsonIgnoreProperties(value = { "createdAt", "modifiedAt" }, allowGetters = true)
public class Rules extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = -3001061281930769374L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private boolean removeDuplicates;
	@Enumerated(EnumType.STRING)
	private ShifPriority shifPriority;
	private Double shiftDifferences;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	@OneToOne
	@JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "FK_rules_organization"))
	private Organization organization;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ShifPriority getShifPriority() {
		return shifPriority;
	}

	public void setShifPriority(ShifPriority shifPriority) {
		this.shifPriority = shifPriority;
	}

	public Double getShiftDifferences() {
		return shiftDifferences;
	}

	public void setShiftDifferences(Double shiftDifferences) {
		this.shiftDifferences = shiftDifferences;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public boolean isRemoveDuplicates() {
		return removeDuplicates;
	}

	public void setRemoveDuplicates(boolean removeDuplicates) {
		this.removeDuplicates = removeDuplicates;
	}

}
