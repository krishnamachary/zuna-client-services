package org.meghdoot.tm.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.Audit.Auditable;
import org.meghdoot.tm.constants.StatusCodeConstants;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "notification")
@JsonIgnoreProperties(value = { "createdAt", "modifiedAt" }, allowGetters = true)
public class Notification extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = -6601946566114484142L;
	
	public Notification() {
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private boolean emailNotificationRequired;
	private String hostName;
	private String port;
	private String userName;
	private String password;
	@ElementCollection
	private List<String> emailIds;
	@Email(message = StatusCodeConstants.INVALID_EMAIL)
	private String fromEmailId;
	@NotBlank(message = StatusCodeConstants.SUBJECT_IS_REQUIRED)
	private String subject;
	private boolean onErrorNotification;
	private boolean dailyNotification;
	private Time dailyNotificationTime;
	private Boolean status;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	@OneToOne
	@JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "FK_notification_organization"))
	private Organization organization;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isEmailNotificationRequired() {
		return emailNotificationRequired;
	}

	public void setEmailNotificationRequired(boolean emailNotificationRequired) {
		this.emailNotificationRequired = emailNotificationRequired;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(List<String> emailIds) {
		this.emailIds = emailIds;
	}

	public String getFromEmailId() {
		return fromEmailId;
	}

	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public boolean isOnErrorNotification() {
		return onErrorNotification;
	}

	public void setOnErrorNotification(boolean onErrorNotification) {
		this.onErrorNotification = onErrorNotification;
	}

	public boolean isDailyNotification() {
		return dailyNotification;
	}

	public void setDailyNotification(boolean dailyNotification) {
		this.dailyNotification = dailyNotification;
	}

	public Time getDailyNotificationTime() {
		return dailyNotificationTime;
	}

	public void setDailyNotificationTime(Time dailyNotificationTime) {
		this.dailyNotificationTime = dailyNotificationTime;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
