package org.meghdoot.tm.model;

public class TmContext {
	
	private User user;
	
	private Organization organization;
	
	public TmContext(User user, Organization organization) {
		this.user = user;
		this.organization = organization;
	}
	
	public User getUser() {
		return user;
	}
	
	public Organization getOrganization() {
		return organization;
	}
	
}
