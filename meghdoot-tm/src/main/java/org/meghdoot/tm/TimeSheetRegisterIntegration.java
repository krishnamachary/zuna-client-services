package org.meghdoot.tm;

import java.io.File;

import org.meghdoot.tm.dto.Email;
import org.meghdoot.tm.service.TMService;
import org.meghdoot.tm.service.impl.ValidationProcessing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.metadata.PropertiesPersistingMetadataStore;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

public class TimeSheetRegisterIntegration {

	Logger log = LoggerFactory.getLogger(TimeSheetRegisterIntegration.class);

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	@Autowired
	ValidationProcessing validationProcessing;

	@Autowired
	TMService tmService;
	
	@Autowired CompositeFileListFilter compositeFilter;
    
    @Autowired MessageChannel filesOut;

    public void processFile(Message<String> filePath) {
    	FileReadingMessageSource fileSource = new FileReadingMessageSource();
        fileSource.setDirectory(new File(filePath.getPayload()));
        fileSource.setFilter(compositeFilter);
        fileSource.afterPropertiesSet();
        fileSource.start();
        new PropertiesPersistingMetadataStore();
        Message<File> msg;
        while ((msg = fileSource.receive()) != null) {
        	log.info(Thread.currentThread().getName()+"--- Processing file ---"+msg.getPayload().getAbsolutePath());
        	filesOut.send(msg);
        }
        fileSource.stop();
	}

	public File copyFile(Message<File> message) {
		File file = message.getPayload();
		System.out.println("Copying file :: " + file);
		return file;
	}

	public Message<String> storeInStaging(Message<File> message) throws JobExecutionAlreadyRunningException,
			JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		File file = message.getPayload();
		System.out.println("file to store in staging ::" + file);
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.toJobParameters();
		JobExecution execution = jobLauncher.run(job, jobParameters);
		log.info(String.format("Exit Status : %s for file name %s", execution.getStatus(), file.getAbsolutePath()));
		String payLoad = "processValidation";
		return MessageBuilder.withPayload(payLoad).setHeader("nextStage", payLoad).build();
	}

	public Message<String> doValidation() {
		String validationResp = validationProcessing.validateTimeSheet();
		return MessageBuilder.withPayload(validationResp).setHeader("nextStage", validationResp).build();
	}

	public Message<String> moveSuccessToDetails() {
		String response = tmService.moveSuccessToDetails();
		return MessageBuilder.withPayload(response).setHeader("nextStage", response).build();
	}

	public Message processAndSendMail(Message emailmessage) {
		//Email email = message.getPayload();
		Email email = new Email();
		email.setFrom("krishnamachary.67@gmail.com");
		email.setTo(new String[]{"krishnamachary.7777@gmail.com"});
		email.setSubject("Hi");
		String body = (String) email.getMessage();
		SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(email.getTo()); 
        message.setSubject(email.getSubject()); 
        message.setText(String.valueOf(body));
		MailSender mailSender = tmService.getJavaMailSender();
		mailSender.send(message);
		return emailmessage;
	}

}
