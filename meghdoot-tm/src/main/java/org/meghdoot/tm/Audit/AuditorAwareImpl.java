package org.meghdoot.tm.Audit;

import java.util.Optional;

import org.meghdoot.tm.session.UserPrincipal;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
    public Optional<String> getCurrentAuditor() {
		UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(userPrincipal.getUsername());
    }

}
