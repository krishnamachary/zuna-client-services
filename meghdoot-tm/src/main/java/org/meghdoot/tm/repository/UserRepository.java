package org.meghdoot.tm.repository;

import java.util.List;

import org.meghdoot.tm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query(value="select u from User u where u.organization.id=?1")
	public List<User> findByOrganization(Long organizationId);
	
	@Query(value="select u from User u where u.organization.id=?1 and u.userName = ?2")
	public User findUser(Long organizationId, String userName);

	@Query(value="select u from User u where u.userName = ?1")
	public User findByUserName(String userName); 

}
