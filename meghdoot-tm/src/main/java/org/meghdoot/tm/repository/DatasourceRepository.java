package org.meghdoot.tm.repository;

import org.meghdoot.tm.model.DataSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DatasourceRepository extends JpaRepository<DataSource, Long> {
	
	@Query("from DataSource n where n.organization.id = :id")
	DataSource findByOrgId(@Param("id") Long id);

}
