package org.meghdoot.tm.repository;

import org.meghdoot.tm.model.Cloud;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CloudPersistence extends GenericRepository<Cloud, Long>{
	@Query("from Cloud c where c.organization.id = :id")
	Cloud findByOrgId(@Param("id") Long id);
	
}
