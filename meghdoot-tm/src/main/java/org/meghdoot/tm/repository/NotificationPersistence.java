package org.meghdoot.tm.repository;

import org.meghdoot.tm.model.Notification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NotificationPersistence extends GenericRepository<Notification, Long>{
	@Query("from Notification n where n.organization.id = :id")
	Notification findByOrgId(@Param("id") Long id);
}
