package org.meghdoot.tm.repository;

import org.meghdoot.tm.model.Rules;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RulesPersistence extends GenericRepository<Rules, Long> {
	@Query("from Rules r where r.organization.id = :id")
	Rules findByOrgId(@Param("id") Long id);
}
