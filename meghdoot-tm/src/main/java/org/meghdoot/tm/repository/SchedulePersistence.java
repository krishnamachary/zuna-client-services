package org.meghdoot.tm.repository;

import org.meghdoot.tm.model.Schedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SchedulePersistence extends GenericRepository<Schedule, Long> {
	@Query("from Schedule s where s.organization.id = :id")
	Schedule findByOrgId(@Param("id") Long id);
}
