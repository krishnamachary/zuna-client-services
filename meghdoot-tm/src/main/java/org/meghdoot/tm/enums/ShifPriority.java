package org.meghdoot.tm.enums;

import org.meghdoot.tm.constants.PropertyConstants;

public enum ShifPriority {
	FIFO(PropertyConstants.FIFO), FILO(PropertyConstants.FILO), LIFO(
			PropertyConstants.LIFO), LILO(PropertyConstants.LILO);

	private final String priority;

	private ShifPriority(String prioritry) {
		this.priority = prioritry;
	}

	public String getValue() {
		return priority;
	}
}
