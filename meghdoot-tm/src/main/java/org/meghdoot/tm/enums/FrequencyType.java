package org.meghdoot.tm.enums;

import org.meghdoot.tm.constants.PropertyConstants;

public enum FrequencyType {
	ONCE(PropertyConstants.ONCE), MINUTES_HOURS(PropertyConstants.MINUTES_HOURS), DAILY(
			PropertyConstants.DAILY), WEEK_BI_WEEKLY(
			PropertyConstants.WEEK_BI_WEEKLY), MONTHLY(
			PropertyConstants.MONTHLY);

	private final String frequency;

	private FrequencyType(String frequency) {
		this.frequency = frequency;
	}

	public String getValue() {
		return frequency;
	}
}
