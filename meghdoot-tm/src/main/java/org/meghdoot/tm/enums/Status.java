package org.meghdoot.tm.enums;

public enum Status {

	PENDING("1"),
	VSUCCESS("2"),
	VERROR("3"),
	COMPLETED("4");
	
	private String strStaus;

	Status(String url) {
        this.strStaus = url;
    }

    public String getStatus() {
        return strStaus;
    }
	
	
}
