package org.meghdoot.tm;

import java.io.File;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.meghdoot.tm.model.DataSource;
import org.meghdoot.tm.repository.DatasourceRepository;
import org.meghdoot.tm.util.ApplicationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.integration.endpoint.SourcePollingChannelAdapter;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class CustomConfiguration {
	
	@Autowired ApplicationContext context;
	
	@Autowired DatasourceRepository datasourceRepository;
	
	@PostConstruct
	public void buildConnectionPools() {
		List<DataSource> dataSources = datasourceRepository.findAll();
		if(!dataSources.isEmpty()) {
			ApplicationUtil.dataSourceMap = new ConcurrentHashMap<>();
			dataSources.stream().forEach(dataSource ->{
				ApplicationUtil.onDemandConnection(dataSource);
			});
		}
	}
	
	
	
	@Bean
	@Scope(value="prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public JdbcTemplate jdbcTemplate() {
		HikariDataSource hikariDataSource = ApplicationUtil.dataSourceMap.get(TmContextLocal.get().getUser().getOrganization().getId());
		return new JdbcTemplate(hikariDataSource);
	}
	
	@Bean
	@Scope(value="prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public FileReadingMessageSource fileMessageSource() {
		SourcePollingChannelAdapter fileInboundAdapter = (SourcePollingChannelAdapter) context
				.getBean("filesIn");
		if (fileInboundAdapter.isRunning())
			fileInboundAdapter.stop();
		
		FileReadingMessageSource source = (FileReadingMessageSource) fileInboundAdapter.getMessageSource();
		source.setDirectory(new File("/input1"));
		fileInboundAdapter.start();
		return source;
	}
	

}
