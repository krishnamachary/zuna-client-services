package org.meghdoot.tm.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.enums.FrequencyType;

public class ScheduleDTO extends BaseDTO {

	private static final long serialVersionUID = 8947601192999046L;
	private Long id;
	@NotBlank(message = StatusCodeConstants.FILE_PATH_IS_REQUIRED)
	private String filePath;
	private String frequencyType;
	private String cronExpression;
	private List<String> frequencySubTypes;
	private Date startTime;
	private Date endTime; 
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	private OrganizationDTO organization;
	private String everyInterval;
	private String hourInterval;
	private String minuteInterval;
	private String monthlyDay;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFrequencyType() {
		return frequencyType;
	}

	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public List<String> getFrequencySubTypes() {
		return frequencySubTypes;
	}

	public void setFrequencySubTypes(List<String> frequencySubTypes) {
		this.frequencySubTypes = frequencySubTypes;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

	public String getEveryInterval() {
		return everyInterval;
	}

	public void setEveryInterval(String everyInterval) {
		this.everyInterval = everyInterval;
	}

	public String getHourInterval() {
		return hourInterval;
	}

	public void setHourInterval(String hourInterval) {
		this.hourInterval = hourInterval;
	}

	public String getMinuteInterval() {
		return minuteInterval;
	}

	public void setMinuteInterval(String minuteInterval) {
		this.minuteInterval = minuteInterval;
	}

	public String getMonthlyDay() {
		return monthlyDay;
	}

	public void setMonthlyDay(String monthlyDay) {
		this.monthlyDay = monthlyDay;
	}

}
