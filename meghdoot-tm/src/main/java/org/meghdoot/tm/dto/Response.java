package org.meghdoot.tm.dto;

import java.util.HashMap;
import java.util.Map;

public class Response {

	private Boolean status;
	private String statusCode;
	private String message;
	private String technicalMessage;
	private Map<String, Object> data;

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTechnicalMessage() {
		return technicalMessage;
	}

	public void setTechnicalMessage(String technicalMessage) {
		this.technicalMessage = technicalMessage;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	public void setProperty(String key,Object value){
		if(data == null){
			data = new HashMap<>();
		}
		data.put(key, value);
	}

}
