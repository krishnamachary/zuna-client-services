package org.meghdoot.tm.dto;

import java.util.Date;
import java.util.List;

public class UserDTO {

	private static final long serialVersionUID = 7081451440337027809L;

	private Long id;

	private String userName;

	private String emailId;

	private String firstName;

	private String lastName; 

	private OrganizationDTO organization;

	private List<AuthoritiesDTO> authorities;

	private Boolean enabled;

	private String mobileNo;
	
	private Date currentLoginDate;

	private Date lastLoginDate;
	
	private String password;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	} 

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCurrentLoginDate() {
		return currentLoginDate;
	}

	public void setCurrentLoginDate(Date currentLoginDate) {
		this.currentLoginDate = currentLoginDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

	public List<AuthoritiesDTO> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<AuthoritiesDTO> authorities) {
		this.authorities = authorities;
	} 
	
}
