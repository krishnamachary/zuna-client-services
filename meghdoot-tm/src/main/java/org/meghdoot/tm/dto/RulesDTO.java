package org.meghdoot.tm.dto;

import javax.validation.constraints.NotNull;

import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.enums.ShifPriority;

public class RulesDTO extends BaseDTO {

	private static final long serialVersionUID = -5120438956821863458L;
	private Long id;
	private ShifPriority shifPriority;
	private Double shiftDifferences;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	private OrganizationDTO organization;
	private boolean removeDuplicates;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ShifPriority getShifPriority() {
		return shifPriority;
	}

	public void setShifPriority(ShifPriority shifPriority) {
		this.shifPriority = shifPriority;
	}

	public Double getShiftDifferences() {
		return shiftDifferences;
	}

	public void setShiftDifferences(Double shiftDifferences) {
		this.shiftDifferences = shiftDifferences;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

	public boolean isRemoveDuplicates() {
		return removeDuplicates;
	}

	public void setRemoveDuplicates(boolean removeDuplicates) {
		this.removeDuplicates = removeDuplicates;
	}

}
