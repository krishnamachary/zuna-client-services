package org.meghdoot.tm.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.constants.StatusCodeConstants;

public class CloudDTO extends BaseDTO {

	private static final long serialVersionUID = -6609095001339666662L;

	public CloudDTO() {
	}

	private Long id;
	@NotBlank(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	private String organizationName;
	private String instanceUrl;
	private String webServiceUrl;
	private String userName;
	private String password;
	private Boolean status;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	private OrganizationDTO organization;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public String getWebServiceUrl() {
		return webServiceUrl;
	}

	public void setWebServiceUrl(String webServiceUrl) {
		this.webServiceUrl = webServiceUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

}
