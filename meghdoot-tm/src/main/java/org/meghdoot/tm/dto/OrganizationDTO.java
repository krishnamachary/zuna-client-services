package org.meghdoot.tm.dto;

import javax.validation.constraints.NotBlank;

import org.meghdoot.tm.constants.StatusCodeConstants;

public class OrganizationDTO extends BaseDTO {

	private static final long serialVersionUID = 3504377527883377810L;
	private Long id;
	@NotBlank(message = StatusCodeConstants.ORG_NAME_IS_REQUIRED)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
