package org.meghdoot.tm.dto;

import java.io.Serializable;

public class Email implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2227652227332143229L;

	private String from;
	private String[] to;
	private String subject;
	private String[] cc;
	private String attachmentFilename;
	private Object message;

	public Email() {
		super();
	}

	public Email(String from, String[] to, String subject, String[] cc, String attachmentFilename, Object message) {
		super();
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.cc = cc;
		this.attachmentFilename = attachmentFilename;
		this.message = message;
	}

	public Object getMessage() {
		return message;
	}

	public void setMessage(Object message) {
		this.message = message;
	}

	public String getFrom() {
		return from;
	}

	public String[] getTo() {
		return to;
	}

	public String getSubject() {
		return subject;
	}

	public String[] getCc() {
		return cc;
	}

	public String getAttachmentFilename() {
		return attachmentFilename;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String[] to) {
		this.to = to;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setCc(String[] cc) {
		this.cc = cc;
	}

	public void setAttachmentFilename(String attachmentFilename) {
		this.attachmentFilename = attachmentFilename;
	}

}
