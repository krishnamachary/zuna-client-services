package org.meghdoot.tm.dto;

import java.io.Serializable;

public class AuthoritiesDTO implements Serializable {
	
	private static final long serialVersionUID = -5222453311184637165L;

	private Long id;
	
	private String authority;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
}
