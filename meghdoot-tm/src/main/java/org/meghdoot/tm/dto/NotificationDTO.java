package org.meghdoot.tm.dto;

import java.sql.Time;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.meghdoot.tm.constants.StatusCodeConstants;

public class NotificationDTO extends BaseDTO {

	private static final long serialVersionUID = 1082470580839052005L;
	private Long id;
	private boolean emailNotificationRequired;
	private String hostName;
	private String port;
	private String userName;
	private String password;
	private List<String> emailIds;
	@Email(message = StatusCodeConstants.INVALID_EMAIL)
	private String fromEmailId;
	@NotBlank(message = StatusCodeConstants.SUBJECT_IS_REQUIRED)
	private String subject;
	private boolean onErrorNotification;
	private boolean dailyNotification;
	private Time dailyNotificationTime;
	private Boolean status;
	@NotNull(message = StatusCodeConstants.ORGANIZATION_IS_REQUIRED)
	private OrganizationDTO organization;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isEmailNotificationRequired() {
		return emailNotificationRequired;
	}

	public void setEmailNotificationRequired(boolean emailNotificationRequired) {
		this.emailNotificationRequired = emailNotificationRequired;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(List<String> emailIds) {
		this.emailIds = emailIds;
	}

	public String getFromEmailId() {
		return fromEmailId;
	}

	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public boolean isOnErrorNotification() {
		return onErrorNotification;
	}

	public void setOnErrorNotification(boolean onErrorNotification) {
		this.onErrorNotification = onErrorNotification;
	}

	public boolean isDailyNotification() {
		return dailyNotification;
	}

	public void setDailyNotification(boolean dailyNotification) {
		this.dailyNotification = dailyNotification;
	}

	public Time getDailyNotificationTime() {
		return dailyNotificationTime;
	}

	public void setDailyNotificationTime(Time dailyNotificationTime) {
		this.dailyNotificationTime = dailyNotificationTime;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

}
