package org.meghdoot.tm.dto;

import java.io.Serializable;

public class TimeSheetSourceDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4398050815438116158L;

	private String emplId;
	
	private String sourceType;
	
	private String sourceId;
	
	private String action;
	
	private String date;
	
	private String time;
	
	private String name;

	@Override
	public String toString() {
		return "TimeSheetSourceDTO [emplId=" + emplId + ", sourceType=" + sourceType + ", sourceId=" + sourceId
				+ ", action=" + action + ", date=" + date + ", time=" + time + ", name=" + name + "]";
	}

	public String getEmplId() {
		return emplId;
	}

	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
