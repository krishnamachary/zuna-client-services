package org.meghdoot.tm.interceptor;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.meghdoot.tm.TmContextLocal;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.model.TmContext;
import org.meghdoot.tm.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class TMContextInterceptor implements HandlerInterceptor {

	@Autowired
	OrganizationRepository organizationRepository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String organizationId = request.getHeader("organizationId");
		if(StringUtils.isEmpty(organizationId)) throw new Exception("Organization should not be empty in request header");
		// TODO Handle inmemory instead of database call
		
		Optional<Organization> organization = organizationRepository.findById(Long.valueOf(organizationId));
		// TODO Need to set user instead of null
		TmContext context = new TmContext(null, organization.get());
		TmContextLocal.setContext(context);
		return true;
	}

}
