package org.meghdoot.tm.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.meghdoot.tm.dto.AuthoritiesDTO;
import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.model.Authorities;
import org.meghdoot.tm.model.Organization;

public class UserVO implements Serializable {

	private static final long serialVersionUID = 8869228871820155272L;
	
	private Long id;

	private String userName;

	private String emailId;

	private String firstName;

	private String lastName; 

	private OrganizationDTO organization;

	private List<AuthoritiesDTO> authorities;

	private Boolean enabled;

	private String mobileNo;
	
	private Date currentLoginDate;

	private Date lastLoginDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCurrentLoginDate() {
		return currentLoginDate;
	}

	public void setCurrentLoginDate(Date currentLoginDate) {
		this.currentLoginDate = currentLoginDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

	public List<AuthoritiesDTO> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<AuthoritiesDTO> authorities) {
		this.authorities = authorities;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	} 

}
