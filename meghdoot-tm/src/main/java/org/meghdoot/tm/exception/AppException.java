package org.meghdoot.tm.exception;

public class AppException extends Exception {

	private static final long serialVersionUID = 4869293143991121092L;

	public AppException() {
		super();
	}
	
	public AppException(String msg){
		super(msg);
	}
	
	public AppException(Throwable e){
		super(e);
	}
}
