package org.meghdoot.tm.exception;

public class NoSuchUserException extends Exception {

	private static final long serialVersionUID = -261635264635865815L;

	public NoSuchUserException() {
		super();
	}
	
	public NoSuchUserException(String msg){
		super(msg);
	}
	
	public NoSuchUserException(Throwable e){
		super(e);
	}
}
