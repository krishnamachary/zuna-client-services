package org.meghdoot.tm;

import org.meghdoot.tm.model.TmContext;

public class TmContextLocal {

	private static ThreadLocal<TmContext> tmContext = new ThreadLocal<TmContext>();
	
	public static void setContext(TmContext tmContext) {
		TmContextLocal.tmContext.set(tmContext);
	}
	
	public static TmContext get() {
		return tmContext.get();
	}
	
}
