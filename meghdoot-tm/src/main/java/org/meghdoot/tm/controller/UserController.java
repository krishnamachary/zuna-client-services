package org.meghdoot.tm.controller;

import java.util.List;

import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.dto.UserDTO;
import org.meghdoot.tm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired UserService userService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public List<UserDTO> getUsersByOrganization(@RequestHeader String organizationId) {
		return userService.getAllUsers();
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/active/inactive")
	public Response activeOrInactive(@RequestBody UserDTO userDTO) {
		Response response = new Response();
		Boolean status = userService.activeOrInactiveUser(userDTO);
		response.setStatus(status);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/reset")
	public Response resetPassword(@RequestBody UserDTO userDTO) {
		Response response = new Response();
		Boolean status = userService.resetPassword(userDTO);
		response.setStatus(status);
		return response;
	}
	
}
