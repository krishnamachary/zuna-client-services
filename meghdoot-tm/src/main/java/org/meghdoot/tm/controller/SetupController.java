package org.meghdoot.tm.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.meghdoot.tm.constants.PropertyConstants;
import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.facade.UserFacade;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.model.User;
import org.meghdoot.tm.service.OrganizationService;
import org.meghdoot.tm.service.UserService;
import org.meghdoot.tm.session.UserPrincipal;
import org.meghdoot.tm.session.UserSession;
import org.meghdoot.tm.util.ApplicationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/setup")
public class SetupController {

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserFacade userFacade;

	@PostMapping(value = "/createOrg")
	public Organization createOrg(@RequestBody Organization organization) {
		User adminUser = userService.createAdmin();
		return organizationService.createOraganization(organization, adminUser);

	}

	@GetMapping(path = { "/getUser" })
	public Response user(Principal principal) {
		Response response = new Response();
		if (principal != null) {
			UserPrincipal userVO = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			response.setProperty(PropertyConstants.USER, userVO);
		}
		return response;
	}

	@PostMapping("/authenticate")
	public Response authenticate(HttpServletRequest request) {
		String emailId = request.getParameter("username");
		String password = request.getParameter("password");
		String orgId = request.getParameter("orgId");
		Response status = userFacade.authenticate(emailId, password);
		if (status.getStatus()) {
			UserSession userSession = ApplicationUtil.buildCurrentUserSession();
			SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			HttpSession session = request.getSession();
			request.getSession().setAttribute(PropertyConstants.USER_SESSION, userSession);
		}
		return status;
	}
}
