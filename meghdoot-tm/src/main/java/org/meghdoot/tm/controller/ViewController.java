package org.meghdoot.tm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {
   
   /**
    * Angular Routing works only if this exits 
    */
   @RequestMapping(value = "/**/{[path:[^\\.]*}")
   public String index() {
       return "forward:/index.html";
   }
}