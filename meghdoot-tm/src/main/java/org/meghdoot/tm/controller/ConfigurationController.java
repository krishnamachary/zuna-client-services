package org.meghdoot.tm.controller;

import javax.websocket.server.PathParam;

import org.meghdoot.tm.dto.CloudDTO;
import org.meghdoot.tm.dto.NotificationDTO;
import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.dto.RulesDTO;
import org.meghdoot.tm.dto.ScheduleDTO;
import org.meghdoot.tm.facade.ConfigurationFacade;
import org.meghdoot.tm.model.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/configuration")
public class ConfigurationController {

	@Autowired
	private ConfigurationFacade configurationFacade;

	@RequestMapping(method = RequestMethod.POST, value = "/saveCloudConfig")
	public Response saveCloudConfig(@RequestBody CloudDTO cloudConfig) {
		return configurationFacade.saveCloudConfig(cloudConfig);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveNotificationConfig")
	public Response saveNotificationConfig(
			@RequestBody NotificationDTO notificationConfig) {
		return configurationFacade.savaNotificationConfig(notificationConfig);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/savaScheduleConfig")
	public Response savaScheduleConfig(@RequestBody ScheduleDTO scheduleConfig) {
		return configurationFacade.saveScheduleConfig(scheduleConfig);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveRulesConfig")
	public Response savaRulesConfig(@RequestBody RulesDTO rulesConfig) {
		return configurationFacade.savaRulesConfig(rulesConfig);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveDatasource")
	public Response savaRulesConfig(@RequestBody DataSource datasource) {
		return configurationFacade.savaDatasource(datasource);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getCloudConfigByOrganization")
	public Response getCloudConfigByOrganization(
			@PathParam(value = "id") String id) {
		return configurationFacade.getCloudConfigByOrganization(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getNotificationConfigByOrganization")
	public Response getNotificationConfigByOrganization(
			@PathParam(value = "id") String id) {
		return configurationFacade.getNotificationConfigByOrganization(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getRulesConfigByOrganization")
	public Response getRulesConfigByOrganization(
			@PathParam(value = "id") String id) {
		return configurationFacade.getRulesConfigByOrganization(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getScheduleConfigByOrganization")
	public Response getScheduleConfigByOrganization(
			@PathParam(value = "id") String id) {
		return configurationFacade.getScheduleConfigByOrganization(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getDatasourceByOrganization")
	public Response getDatasourceByOrganization(
			@PathParam(value = "id") String id) {
		return configurationFacade.getDatasourceByOrganization(id);
	}
}
