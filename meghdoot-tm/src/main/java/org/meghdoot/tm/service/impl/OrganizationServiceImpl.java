package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.model.User;
import org.meghdoot.tm.repository.OrganizationRepository;
import org.meghdoot.tm.service.OrganizationService;
import org.meghdoot.tm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	
	@Autowired private OrganizationRepository organizationRepository; 
	
	@Autowired private UserService userService;

	@Override
	public Organization createOraganization(Organization org, User adminUser) {
		organizationRepository.save(org);
		adminUser.setOrganization(org);
		userService.createUser(adminUser);
		return org;
	}

}
