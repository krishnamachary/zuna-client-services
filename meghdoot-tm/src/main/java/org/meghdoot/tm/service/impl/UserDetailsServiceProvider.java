package org.meghdoot.tm.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.meghdoot.tm.model.Authorities;
import org.meghdoot.tm.model.User;
import org.meghdoot.tm.repository.UserRepository;
import org.meghdoot.tm.session.UserPrincipal;
import org.meghdoot.tm.util.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceProvider implements UserDetailsService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserDetailsServiceProvider.class);
	
	@Autowired
    private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		/*String[] userTokens = userName_OrgId.split(":");
		String userName = null;
		String organizationId = null;
		try {
			userName = userTokens[0];
			organizationId = userTokens[1];
		} catch (Exception e) {
			LOG.error("Exception at loadUserByUsername:" + e);
			throw new UsernameNotFoundException("user '"+ userName +"' not found");
		}*/
		User user = authenticateUser(userName);
		Set<GrantedAuthority> grantedAuthority = new HashSet<>();
		List<Authorities> roles = user.getAuthorities();
		if (roles.isEmpty())
			throw new UsernameNotFoundException("User '"+ userName +"' has no GrantedAuthority");
		for (Authorities authority : roles) {
			grantedAuthority.add(new SimpleGrantedAuthority(authority.getAuthority()));
		}
		return buildUser(user, grantedAuthority);
	}
	
	private UserDetails buildUser(User user, Set<GrantedAuthority> grantedAuthority) {
		return new UserPrincipal(user.getUserName()+":"+user.getOrganization().getId(), user.getPassword(), user.getEnabled(), 
				Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, grantedAuthority, user.getUserId(), user.getFirstName(), user.getLastName(),
				user.getMobileNo(), null, user.getEmailId());
	}

	private User authenticateUser(String userName) {
		User user = userRepository.findByUserName(userName);
		if (user == null)
			throw new UsernameNotFoundException("Username '"+userName+"' not found");
		if (!user.getEnabled()) {
			throw new DisabledException(ApplicationUtil.getMessage("user.disabled", null));
		}
		return user;
	}

}
