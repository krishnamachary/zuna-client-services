package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.CloudDTO;
import org.meghdoot.tm.exception.AppException;

public interface CloudService {
	public void persistCloudConfig(CloudDTO cloudConfig) throws AppException;
	public CloudDTO getCloudConfigByOrganization(String id) throws AppException;
}
