package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.Email;

public interface EmailService {
	
	public void sendSimpleMessage(Email mail);

}
