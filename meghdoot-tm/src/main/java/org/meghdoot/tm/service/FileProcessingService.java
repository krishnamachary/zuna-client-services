package org.meghdoot.tm.service;

public interface FileProcessingService {
	
	public void processFiles(String filePath);

}
