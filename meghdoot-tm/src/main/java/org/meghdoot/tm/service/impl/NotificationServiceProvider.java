package org.meghdoot.tm.service.impl;


import org.meghdoot.tm.dto.NotificationDTO;
import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.Notification;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.repository.NotificationPersistence;
import org.meghdoot.tm.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceProvider implements NotificationService {
	private static final Logger LOG = LoggerFactory.getLogger(NotificationServiceProvider.class);
	
	@Autowired
	private NotificationPersistence notificationPersistence;
	
	@Override
	public void persistNotificationConfig(NotificationDTO notificationDTO) throws AppException {
		try {
			Notification notification = new Notification();
			BeanUtils.copyProperties(notificationDTO, notification);
			Organization  org = new Organization();
			BeanUtils.copyProperties(notificationDTO.getOrganization(), org);
			notification.setOrganization(org);
			notificationPersistence.save(notification);
		} catch (Exception e) {
			LOG.error("Exception occured at persistNotificationConfig", e);
			LOG.info("Exception occured at persistNotificationConfig", e);
			throw new AppException();
		}		
	}
	
	@Override
	public NotificationDTO getNotificationConfigByOrganization(String id) throws AppException {
		NotificationDTO notificationDTO = new NotificationDTO();
		try {
			Notification notification = notificationPersistence.findByOrgId(Long.valueOf(id));
			if (notification!= null) {
				BeanUtils.copyProperties(notification, notificationDTO);
				OrganizationDTO organizationDTO = new OrganizationDTO();
				BeanUtils.copyProperties(notification.getOrganization(), organizationDTO);
				notificationDTO.setOrganization(organizationDTO);
			}
		} catch (Exception e) {
			LOG.error("Exception occured at getNotificationConfigByOrganization", e);
			LOG.info("Exception occured at getNotificationConfigByOrganization", e);
			throw new AppException();
		}
		return notificationDTO;
	}

}
