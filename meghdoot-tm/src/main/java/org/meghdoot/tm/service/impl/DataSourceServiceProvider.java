package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.dto.DatasourceDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.DataSource;
import org.meghdoot.tm.repository.DatasourceRepository;
import org.meghdoot.tm.service.DataSourceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataSourceServiceProvider implements DataSourceService {
	
	@Autowired DatasourceRepository datasourceRepository;

	@Override
	public void persistDataSource(DataSource dataSource) throws AppException {
		datasourceRepository.save(dataSource);
	}

	@Override
	public DatasourceDTO getDatasourceByOrganization(Long id) throws AppException {
		DataSource dataSource = datasourceRepository.findByOrgId(id);
		DatasourceDTO datasourceDTO = new DatasourceDTO();
		if(dataSource != null) {
			BeanUtils.copyProperties(dataSource, datasourceDTO);
		}
		return datasourceDTO;
	}

}
