package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.ScheduleDTO;
import org.meghdoot.tm.exception.AppException;

public interface ScheduleService {
	public void persistScheduleConfig(ScheduleDTO scheduleConfig) throws AppException;
	public ScheduleDTO getScheduleConfigByOrganization(String id) throws AppException;
}
