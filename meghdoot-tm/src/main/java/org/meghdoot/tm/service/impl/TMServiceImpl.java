package org.meghdoot.tm.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.meghdoot.tm.custom.model.TimeCardDetails;
import org.meghdoot.tm.custom.model.TimeSheetSource;
import org.meghdoot.tm.custom.repository.TimeCardDetailsRepository;
import org.meghdoot.tm.custom.repository.TimeSheetSourceRepository;
import org.meghdoot.tm.enums.Status;
import org.meghdoot.tm.service.TMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class TMServiceImpl implements TMService {
	
	@Autowired TimeSheetSourceRepository timeSheetSourceRepository;
	
	@Autowired TimeCardDetailsRepository timeCardDetailsRepository;

	public String moveSuccessToDetails() {
		List<TimeSheetSource> sheetSources = timeSheetSourceRepository.getAllSources(Status.VSUCCESS.getStatus());
		List<TimeCardDetails> cardDetails = sheetSources.stream().map(sheetSource -> {
			TimeCardDetails cardDetail = new TimeCardDetails();
			cardDetail.setEmpId(sheetSource.getEmpId());
			cardDetail.setLegacyEmpId(sheetSource.getLegacyEmpId());
			cardDetail.setSourceName(sheetSource.getSourceName());
			cardDetail.setSourceType(sheetSource.getSourceType());
			cardDetail.setAction(sheetSource.getAction());
			cardDetail.setDate(sheetSource.getDate());
			cardDetail.setRequestStatus(sheetSource.getRequestStatus());
			//TODO Need to set other attributes by configuration
			cardDetail.setCreatedDate(Calendar.getInstance().getTime());
			return cardDetail;
		}).collect(Collectors.toList());
		timeCardDetailsRepository.saveAll(cardDetails);
		return "success";
	}
	
	public JavaMailSender getJavaMailSender() {
	    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    // TODO laod from configuration
	    mailSender.setHost("smtp.gmail.com");
	    mailSender.setPort(587);
	     
	    mailSender.setUsername("krishnamachary.67@gmail.com");
	    mailSender.setPassword("Student_67");
	     
	    Properties properties = mailSender.getJavaMailProperties();
	     
	    properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");
		// properties.put("mail.debug", "true");
	    return mailSender;
	}
}
