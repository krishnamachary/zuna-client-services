package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.dto.ScheduleDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.model.Schedule;
import org.meghdoot.tm.repository.SchedulePersistence;
import org.meghdoot.tm.service.ScheduleService;
import org.meghdoot.tm.util.UniqueKeyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceProvider implements ScheduleService {
	private static final Logger LOG = LoggerFactory.getLogger(ScheduleServiceProvider.class);
	
	@Autowired
	private SchedulePersistence schedulePersistence;
	
	@Override
	public void persistScheduleConfig(ScheduleDTO scheduleDTO) throws AppException {
		try {
			Schedule schedule = new Schedule();
			BeanUtils.copyProperties(scheduleDTO, schedule);
			Organization  org = new Organization();
			BeanUtils.copyProperties(scheduleDTO.getOrganization(), org);
			schedule.setOrganization(org); 
			schedulePersistence.save(schedule);
		} catch (Exception e) {
			LOG.error("Exception occured at persistScheduleConfig", e);
			LOG.info("Exception occured at persistScheduleConfig", e);
			throw new AppException();
		}
	}

	@Override
	public ScheduleDTO getScheduleConfigByOrganization(String id) throws AppException {
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		try {
			Schedule schedule = schedulePersistence.findByOrgId(Long.valueOf(id));
			if (schedule!= null) {
				BeanUtils.copyProperties(schedule, scheduleDTO);
				OrganizationDTO organizationDTO = new OrganizationDTO();
				BeanUtils.copyProperties(schedule.getOrganization(), organizationDTO);
				scheduleDTO.setOrganization(organizationDTO);
			}
		} catch (Exception e) {
			LOG.error("Exception occured at getScheduleConfigByOrganization", e);
			LOG.info("Exception occured at getScheduleConfigByOrganization", e);
			throw new AppException();
		}
		return scheduleDTO;
	}

}
