package org.meghdoot.tm.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.meghdoot.tm.TmContextLocal;
import org.meghdoot.tm.dto.AuthoritiesDTO;
import org.meghdoot.tm.dto.Email;
import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.dto.UserDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.exception.NoSuchUserException;
import org.meghdoot.tm.facade.UserFacade;
import org.meghdoot.tm.model.Authorities;
import org.meghdoot.tm.model.User;
import org.meghdoot.tm.repository.UserRepository;
import org.meghdoot.tm.service.EmailService;
import org.meghdoot.tm.service.UserService;
import org.meghdoot.tm.util.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOG = LoggerFactory.getLogger(UserFacade.class);

	@Autowired
	UserRepository userRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	EmailService emailService;

	@Override
	public User createUser(User user) {
		userRepository.save(user);
		return user;
	}

	@Override
	public List<UserDTO> getAllUsers() {
		List<User> users = userRepository.findByOrganization(TmContextLocal.get().getOrganization().getId());
		List<UserDTO> usersDTO = new ArrayList<>();
		for (User user : users) {
			UserDTO userDTO = new UserDTO();
			BeanUtils.copyProperties(user, userDTO);
			List<AuthoritiesDTO> authorityDTOList = new ArrayList<>();
			for (Authorities authority : user.getAuthorities()) {
				AuthoritiesDTO aDto = new AuthoritiesDTO();
				BeanUtils.copyProperties(authority, aDto);
				authorityDTOList.add(aDto);
			}
			userDTO.setAuthorities(authorityDTOList);
			OrganizationDTO orgDTO = new OrganizationDTO();
			BeanUtils.copyProperties(user.getOrganization(), orgDTO);

		}
		return usersDTO;
	}

	@Override
	public Boolean activeOrInactiveUser(UserDTO userDTO) {
		Long orgId = TmContextLocal.get().getOrganization().getId();
		User user = userRepository.findUser(orgId, userDTO.getUserName());
		user.setEnabled(userDTO.getEnabled());
		userRepository.save(user);
		return true;
	}

	@Override
	public Boolean resetPassword(UserDTO userDTO) {
		Long orgId = TmContextLocal.get().getOrganization().getId();
		User user = userRepository.findUser(orgId, userDTO.getUserName());
		if (Base64.isBase64(userDTO.getPassword())) {
			user.setPassword(bCryptPasswordEncoder.encode(String.valueOf(Base64.decodeBase64(userDTO.getPassword()))));
			userRepository.save(user);
		} else {
			return false;
		}
		return true;
	}
	
	@Override
	public User createAdmin() {
		User user = new User();
		user.setFirstName("cedar");
		user.setLastName("cedar");
		user.setUserName("cedar");
		String password = PasswordGenerator.generatePassword(6, PasswordGenerator.STANDARD);
		user.setPassword(bCryptPasswordEncoder.encode(password));
		List<Authorities> authorities = new ArrayList<>(1);
		Authorities authority = new Authorities();
		authority.setAuthority("ADMIN");
		authorities.add(authority);
		user.setAuthorities(authorities);
		user.setEnabled(Boolean.TRUE);
		String username = "krishnamachary7777@gmail.com";
		Email mail = new Email(username, new String[]{username}, "Welcome to Meghdoot", null, null, password);
		emailService.sendSimpleMessage(mail);
		return user;
	}

	@Override
	public UserDTO loginUser(String userName, String password) throws NoSuchUserException, AppException {
		UserDTO userDTO = new UserDTO();
		try {
			User user = userRepository.findByUserName(userName);
			if (user == null) {
				throw new NoSuchUserException();
			}
			if (user != null && user.getEnabled() && bCryptPasswordEncoder.matches(password, user.getPassword())) {
				user.setLastLoginDate(user.getCurrentLoginDate());
				user.setCurrentLoginDate(new Date());
				userRepository.save(user);
			} 
			userDTO = buildUserDTO(user);
		} catch (Exception e) {e.printStackTrace();
			LOG.error("Exception occured at loginUser", e);
			LOG.info("Exception occured at loginUser", e);
			throw new AppException();
		}
		return userDTO;
		
	}

	@Override
	public UserDTO findByUserName(String userName) throws AppException {
		UserDTO userDTO = new UserDTO();
		try {
			User user = userRepository.findByUserName(userName);
			userDTO = buildUserDTO(user);
		} catch (Exception e) {
			LOG.error("Exception occured at findByUserName", e);
			LOG.info("Exception occured at findByUserName", e);
			throw new AppException();
		}
		return userDTO;
	}
	
	private UserDTO buildUserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		BeanUtils.copyProperties(user, userDTO);
		List<AuthoritiesDTO> authoritiesDTO = new ArrayList<>();
		for ( Authorities authority : user.getAuthorities()) {
			AuthoritiesDTO authorityDTO = new AuthoritiesDTO();
			BeanUtils.copyProperties(authority, authorityDTO);
			authoritiesDTO.add(authorityDTO);
		}
		OrganizationDTO organizationDTO = new OrganizationDTO();
		BeanUtils.copyProperties(user.getOrganization(), organizationDTO);
		userDTO.setAuthorities(authoritiesDTO);
		userDTO.setOrganization(organizationDTO);
		return userDTO;
	}


}
