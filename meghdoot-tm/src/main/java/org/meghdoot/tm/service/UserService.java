package org.meghdoot.tm.service;

import java.util.List;

import org.meghdoot.tm.dto.UserDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.exception.NoSuchUserException;
import org.meghdoot.tm.model.User;

public interface UserService {
	
	public User createUser(User user);
	
	public User createAdmin();
	
	public List<UserDTO> getAllUsers();
	
	public Boolean activeOrInactiveUser(UserDTO userDTO);
	
	public Boolean resetPassword(UserDTO userDTO);
	
	public UserDTO loginUser(String userName, String password) throws AppException, NoSuchUserException;

	public UserDTO findByUserName(String userName) throws AppException;
	
}
