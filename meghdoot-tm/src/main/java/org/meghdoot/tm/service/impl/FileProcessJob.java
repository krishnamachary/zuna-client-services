package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.service.FileProcessingService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class FileProcessJob extends QuartzJobBean {
	
	@Value("${input.directory}")
	private String filePath;

	private String jobName;
	
	@Autowired private FileProcessingService fileProcessingService;

	public void setName(String name) { 
		this.jobName = name;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		fileProcessingService.processFiles(this.filePath);
		System.out.println("EXECUTING "+jobName);
	}
}
