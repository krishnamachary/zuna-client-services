package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.dto.Email;
import org.meghdoot.tm.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService{

	@Autowired
    public JavaMailSender emailSender;
	
	@Override
	public void sendSimpleMessage(Email mail) {
		SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(mail.getTo()); 
        message.setSubject(mail.getSubject()); 
        message.setText(mail.getMessage().toString());
        emailSender.send(message);
	}

}
