package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.RulesDTO;
import org.meghdoot.tm.exception.AppException;

public interface RulesService {
	public void persistRulesConfig(RulesDTO rulesConfig) throws AppException;
	public RulesDTO getRulesConfigByOrganization(String id) throws AppException;
}
