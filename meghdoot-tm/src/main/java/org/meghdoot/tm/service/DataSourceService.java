package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.DatasourceDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.DataSource;

public interface DataSourceService {
	
	public void persistDataSource(DataSource dataSource) throws AppException;
	public DatasourceDTO getDatasourceByOrganization(Long id) throws AppException;

}
