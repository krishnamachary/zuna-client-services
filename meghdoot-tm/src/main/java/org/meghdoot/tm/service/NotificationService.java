package org.meghdoot.tm.service;

import org.meghdoot.tm.dto.NotificationDTO;
import org.meghdoot.tm.exception.AppException;


public interface NotificationService {
	public void persistNotificationConfig(NotificationDTO notificationConfig) throws AppException;
	public NotificationDTO getNotificationConfigByOrganization(String id) throws AppException;
	
}
