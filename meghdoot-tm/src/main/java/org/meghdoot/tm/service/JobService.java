package org.meghdoot.tm.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.scheduling.quartz.QuartzJobBean;

public interface JobService {
	boolean scheduleOneTimeJob(String jobGroup, String jobName, Class<? extends QuartzJobBean> jobClass, Date date);
	boolean scheduleCronJob(String jobGroup, String jobName, Class<? extends QuartzJobBean> jobClass, Date date, String cronExpression);
	
	boolean updateOneTimeJob(String jobName, Date date);
	boolean updateCronJob(String jobName, Date date, String cronExpression);
	
	boolean unScheduleJob(String jobName);
	boolean deleteJob(String jobName, String jobGroup);
	boolean pauseJob(String jobName, String jobGroup);
	boolean resumeJob(String jobName, String jobGroup);
	boolean startJobNow(String jobName, String jobGroup);
	boolean isJobRunning(String jobName, String jobGroup);
	List<Map<String, Object>> getAllJobs(String jobGroup);
	boolean isJobWithNamePresent(String jobName, String jobGroup);
	String getJobState(String jobName, String jobGroup);
	boolean stopJob(String jobName, String jobGroup);
}
