package org.meghdoot.tm.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.meghdoot.tm.custom.model.TimeSheetSource;
import org.meghdoot.tm.custom.repository.TimeSheetSourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class ValidationProcessing {
	
	private static final String  FIRST = "FIRST";
	private static final String  LAST = "LAST";

	@Autowired
	TimeSheetSourceRepository timeSheetSourceRepository;

	public String validateTimeSheet() {
		List<TimeSheetSource> sheetSources = timeSheetSourceRepository.findAll();
		Map<String, Map<Integer, List<TimeSheetSource>>> empByAction = segregateByEmpAndAction(sheetSources);
		// TODO Set Entries By Config
		return "successValidation";
	}
	
	private List<List<TimeSheetSource>> getAsFIFO(Map<String, Map<Integer, List<TimeSheetSource>>> employeeActions){
		return setEntriesByMode(employeeActions, FIRST, FIRST);
	}
	
	private List<List<TimeSheetSource>> getAsFILO(Map<String, Map<Integer, List<TimeSheetSource>>> employeeActions){
		return setEntriesByMode(employeeActions, FIRST, LAST);
	}
	
	private List<List<TimeSheetSource>> getAsLILO(Map<String, Map<Integer, List<TimeSheetSource>>> employeeActions){
		return setEntriesByMode(employeeActions, LAST, LAST);
	}
	
	private List<List<TimeSheetSource>> getAsLIFO(Map<String, Map<Integer, List<TimeSheetSource>>> employeeActions){
		return setEntriesByMode(employeeActions, LAST, FIRST);
	}
	

	private Map<String, Map<Integer, List<TimeSheetSource>>> segregateByEmpAndAction(List<TimeSheetSource> sheetSources) {
		Map<String, Map<Integer, List<TimeSheetSource>>> empByAction = sheetSources.stream()
				.sorted((e1, e2) -> e1.getDate().compareTo(e2.getDate())).collect(Collectors
						.groupingBy(TimeSheetSource::getEmpId, Collectors.groupingBy(TimeSheetSource::getAction)));
		return empByAction;
	}
	
	private List<List<TimeSheetSource>> setEntriesByMode(Map<String, Map<Integer, List<TimeSheetSource>>> employeeActions, String modeIn, String modeOut){
		return employeeActions.entrySet().stream().map(empByAction -> {
			List<TimeSheetSource> inAndOuts = new ArrayList<>(2); //Size is always two As it hold employee In and out Action
			List<TimeSheetSource> ins = empByAction.getValue().get(1); // 1 is IN. SET As ENUM
			List<TimeSheetSource> outs = empByAction.getValue().get(0); // 0 is out. SET As ENUM
			TimeSheetSource firstIn = getByMode(modeIn, ins, null ,null);
			TimeSheetSource firstOut = getByMode(modeOut, ins, null ,null);
			inAndOuts.add(firstIn); inAndOuts.add(firstOut);
			return inAndOuts;
		}).collect(Collectors.toList());
		
		
	}
	
	private TimeSheetSource getByMode(String mode, List<TimeSheetSource> sheetSources, Date from, Date to) {
		Assert.isNull(mode, "Mode should not be null. Pass either FIRST/LAST");
		List<TimeSheetSource> employeeActions = null;
		if(Objects.isNull(from) && Objects.isNull(to)) {
			employeeActions = sheetSources;
		}
		// TODO Need to work here with clarifications
		switch (mode) {
		case "FIRST":
			return sheetSources.get(0);
		case "LAST":
			return sheetSources.get(sheetSources.size()-1);
		default:
			break;
		}
		return null;
	}

}
