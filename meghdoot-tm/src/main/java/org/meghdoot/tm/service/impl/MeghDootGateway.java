package org.meghdoot.tm.service.impl;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface MeghDootGateway {

	@Gateway(requestChannel = "fileChannel")
	void processFile();

}
