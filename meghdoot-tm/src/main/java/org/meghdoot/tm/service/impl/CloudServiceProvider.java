package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.dto.CloudDTO;
import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.Cloud;
import org.meghdoot.tm.model.Notification;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.repository.CloudPersistence;
import org.meghdoot.tm.service.CloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CloudServiceProvider implements CloudService {
	private static final Logger LOG = LoggerFactory
			.getLogger(CloudServiceProvider.class);
	
	@Autowired
	private CloudPersistence cloudPersistence;
	
	@Override
	public void persistCloudConfig(CloudDTO cloudDTO) throws AppException {
		try { 
			Cloud cloud = new Cloud();
			BeanUtils.copyProperties(cloudDTO, cloud);
			Organization  org = new Organization();
			BeanUtils.copyProperties(cloudDTO.getOrganization(), org);
			cloud.setOrganization(org);
			cloudPersistence.save(cloud); 
		} catch (Exception e) {
			LOG.error("Exception occured at persistCloudConfig", e);
			LOG.info("Exception occured at persistCloudConfig", e);
			throw new AppException();
		}
	} 
	
	@Override
	public CloudDTO getCloudConfigByOrganization(String id) throws AppException {
		CloudDTO cloudDTO = new CloudDTO();
		try {
			Cloud cloud = cloudPersistence.findByOrgId(Long.valueOf(id));
			if (cloud!= null) {
				BeanUtils.copyProperties(cloud, cloudDTO);
				OrganizationDTO organizationDTO = new OrganizationDTO();
				BeanUtils.copyProperties(cloud.getOrganization(), organizationDTO);
				cloudDTO.setOrganization(organizationDTO);
			}
		} catch (Exception e) {
			LOG.error("Exception occured at getCloudConfigByOrganization", e);
			LOG.info("Exception occured at getCloudConfigByOrganization", e);
			throw new AppException();
		}
		return cloudDTO;
	}

}
