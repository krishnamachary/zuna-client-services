package org.meghdoot.tm.service.impl;

import org.meghdoot.tm.dto.OrganizationDTO;
import org.meghdoot.tm.dto.RulesDTO;
import org.meghdoot.tm.dto.ScheduleDTO;
import org.meghdoot.tm.exception.AppException;
import org.meghdoot.tm.model.Organization;
import org.meghdoot.tm.model.Rules;
import org.meghdoot.tm.model.Schedule;
import org.meghdoot.tm.repository.RulesPersistence;
import org.meghdoot.tm.service.RulesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RulesServiceProvider implements RulesService {
	private static final Logger LOG = LoggerFactory
			.getLogger(RulesServiceProvider.class);
	
	@Autowired
	private RulesPersistence rulesPersistence;
	
	@Override
	public void persistRulesConfig(RulesDTO rulesDTO) throws AppException {
		try {
			Rules rules = new Rules();
			BeanUtils.copyProperties(rulesDTO, rules);
			Organization  org = new Organization();
			BeanUtils.copyProperties(rulesDTO.getOrganization(), org);
			rules.setOrganization(org);
			rulesPersistence.save(rules);
		} catch (Exception e) {
			LOG.error("Exception occured at persistRulesConfig", e);
			LOG.info("Exception occured at persistRulesConfig", e);
			throw new AppException();
		}
	}

	@Override
	public RulesDTO getRulesConfigByOrganization(String id) throws AppException {
		RulesDTO rulesDTO = new RulesDTO();
		try {
			Rules rules = rulesPersistence.findByOrgId(Long.valueOf(id));
			if (rules!= null) {
				BeanUtils.copyProperties(rules, rulesDTO);
				OrganizationDTO organizationDTO = new OrganizationDTO();
				BeanUtils.copyProperties(rules.getOrganization(), organizationDTO);
				rulesDTO.setOrganization(organizationDTO);
				
			}
			
		} catch (Exception e) {
			LOG.error("Exception occured at getRulesConfigByOrganization", e);
			LOG.info("Exception occured at getRulesConfigByOrganization", e);
			throw new AppException();
		}
		return rulesDTO;
	}

}
