package org.meghdoot.tm.service;

import org.springframework.mail.javamail.JavaMailSender;

public interface TMService {
	
	public String moveSuccessToDetails();
	
	public JavaMailSender getJavaMailSender();

}
