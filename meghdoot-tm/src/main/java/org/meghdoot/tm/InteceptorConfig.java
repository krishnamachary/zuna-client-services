package org.meghdoot.tm;

import org.meghdoot.tm.interceptor.TMContextInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.GzipResourceResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class InteceptorConfig implements WebMvcConfigurer {

	@Autowired
	TMContextInterceptor tMContextInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(tMContextInterceptor).addPathPatterns("/configuration/**");
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry
	      .addResourceHandler("/resources/static/**")
	      .addResourceLocations("/static/")
	      .setCachePeriod(3600)
	      .resourceChain(true)
	      .addResolver(new GzipResourceResolver())
	      .addResolver(new PathResourceResolver());
	} 
}
