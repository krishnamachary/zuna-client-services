package org.meghdoot.tm.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static String getDateFormat(Date mdate, String pattern) {
		if (mdate != null) {
			if (pattern == null)
				pattern = DATE_TIME_FORMAT;
			DateFormat formatter = new SimpleDateFormat(pattern);
			String date = (String) formatter.format(mdate);
			return date;
		} else {
			return null;
		}
	}
}
