package org.meghdoot.tm.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.meghdoot.tm.constants.StatusCodeConstants;
import org.meghdoot.tm.dto.Response;
import org.meghdoot.tm.model.Authorities;
import org.meghdoot.tm.model.DataSource;
import org.meghdoot.tm.model.User;
import org.meghdoot.tm.session.UserSession;
import org.meghdoot.tm.session.UserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ApplicationUtil {
	
	public static Map<Long, HikariDataSource>	dataSourceMap;
	
	public static User createAdmin() {
		User user = new User();
		user.setFirstName("cedar");
		user.setLastName("cedar");
		user.setUserName("cedar");
		user.setPassword(new BCryptPasswordEncoder().encode("cedar"));
		List<Authorities> authorities = new ArrayList<>(1);
		Authorities authority = new Authorities();
		authority.setAuthority("ADMIN");
		authorities.add(authority);
		user.setAuthorities(authorities);
		user.setEnabled(Boolean.TRUE);
		return user;
	}
	
	public static Response getExceptionBaseResponse(String technicalMessage) {
		Response response = new Response();
		response.setStatus(Boolean.FALSE);
		response.setMessage(getMessage(
				StatusCodeConstants.SOMETHING_WENT_WRONG_TRY_AFTER_SOMETIME, null));
		response.setTechnicalMessage(technicalMessage);
		return response;
	}
	
	public static void onDemandConnection(DataSource dataSource) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(dataSource.getJdbcUrl());
		config.setUsername(dataSource.getUserName());
		config.setPassword(dataSource.getPassword());
		HikariDataSource hikariDataSource = new HikariDataSource(config);
		ApplicationUtil.dataSourceMap.put(dataSource.getOrganization().getId(), hikariDataSource);
	}
	
	public static String getMessage(String code, String[] args) {
		try {
			return code;//messageSource.getMessage(messageCode, descriptionArgs, Locale.ENGLISH);
		} catch (Exception e) {
			return code;
		}
	}
	
	public static UserSession buildCurrentUserSession() {
		UserPrincipal user = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserSession userSession = new UserSession();
		userSession.setEmailId(user.getEmailId());
		userSession.setFirstName(user.getFirstName());
		userSession.setLastName(user.getLastName());
		userSession.setUserId(user.getUserId());
		userSession.setMobileNo(user.getMobileNo());
		return userSession;
	}

	
}
