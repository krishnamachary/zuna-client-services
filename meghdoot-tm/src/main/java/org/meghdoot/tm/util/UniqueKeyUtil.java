package org.meghdoot.tm.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.UUID;

import org.meghdoot.tm.exception.AppException;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;


public class UniqueKeyUtil {

	public static String mac = null;

	static String WINDOWS_OS = "Windows";
	static String LINUX_OS = "Linux";

 
	public static String getUniqueKey() throws AppException {
		try {
			if (mac == null)
				mac = getMac();
			if (mac == null)
				mac = "1C:75:08:7C:61:BA";
			EthernetAddress ethernetAddress = new EthernetAddress(mac);
			UUID uuid = Generators.timeBasedGenerator(ethernetAddress).generate();
			return uuid.toString().replaceAll("-", "");
		} catch (Exception e) {
			throw new AppException(e);
		}
	}

	public static String getUUId() throws AppException {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
 
	public static String getMac() throws Exception {
		if (System.getProperty("os.name").indexOf(WINDOWS_OS) != -1) {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
			}
			return sb.toString();
		} else if (System.getProperty("os.name").indexOf(LINUX_OS) != -1) {
			String macAddress = null;
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface network = networkInterfaces.nextElement();
				byte[] mac = network.getHardwareAddress();
				if (mac != null) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
					}
					macAddress = sb.toString();
					break;
				}
			}
			return macAddress;
		} else {
			return "1C:75:08:7C:61:BA";
		}
	}
}
