package org.meghdoot.tm.util;

import java.security.SecureRandom;

public class PasswordGenerator {
	    private static SecureRandom random = new SecureRandom();

	    /** different dictionaries used */
	    private static final String ALPHA_CAPS = "ABCDEFGHJKLMNPQRSTUVWXYZ";
	    private static final String ALPHA = "abcdefghjklmnpqrstuvwxyz";
	    private static final String NUMERIC = "23456789";
	    private static final String SPECIAL_CHARS = "!@#$%&*+_/";
	    public static final String STANDARD = ALPHA_CAPS.concat(ALPHA).concat(NUMERIC).concat(SPECIAL_CHARS);

	    /**
	     * Method will generate random string based on the parameters
	     * 
	     * @param len
	     *            the length of the random string
	     * @param dic
	     *            the dictionary used to generate the password
	     * @return the random password
	     */
	    public static String generatePassword(Integer len, String dictionary) {
	    String result = "";
	    for (int i = 0; i < len; i++) {
	        int index = random.nextInt(dictionary.length());
	        result += dictionary.charAt(index);
	    }
	    return result;
	    }

}
