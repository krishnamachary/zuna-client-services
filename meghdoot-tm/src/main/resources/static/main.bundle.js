webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-white navbar-transparent\" *ngIf=\"isLoggedIn()\">\n  <div class=\"container\">\n      <a class=\"navbar-brand\" data-scroll href=\"#home\">\n          <img src=\"assets/images/sierra-cedar_logo.png\" alt=\"\" class=\"logo\">\n        </a>\n<ul class=\"nav navbar-nav\">\n  <!-- <li class=\"nav-item\"><a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/home\">Home</a></li>\n  <li class=\"nav-item\"><a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/login\">Login</a></li> --> \n        \n  <li class=\"nav-item\">\n      <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\" >Configuration</a>\n    </li>\n    <li class=\"nav-item\">\n        <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"\" >Users</a>\n      </li>\n      <li class=\"nav-item\">\n          <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"\" >Reports</a>\n        </li>\n      <li class=\"nav-item\">\n        <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"\" (click)=\"logout()\">Logout</a>\n      </li>\n    \n    <!-- <button (click)=\"logout()\">Logout</button>  --> \n  \n</ul>\n\n</div>\n</nav>\n<div class=\"container\">\n<router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login_service__ = __webpack_require__("./src/app/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_finally__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/finally.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(login, http, router) {
        var _this = this;
        this.login = login;
        this.http = http;
        this.router = router;
        var credentials = {};
        this.login.isUserLoogedIn(function (status) {
            if (!status) {
                _this.router.navigateByUrl('/login');
            }
        });
    }
    AppComponent.prototype.logout = function () {
        var _this = this;
        this.http.post('logout', {}).finally(function () {
            _this.login.authenticated = false;
            _this.router.navigateByUrl('/login');
        }).subscribe();
    };
    AppComponent.prototype.isLoggedIn = function () {
        return this.login.authenticated;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__login_login_service__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export XhrInterceptor */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notification_notification_component__ = __webpack_require__("./src/app/notification/notification.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__cloud_cloud_component__ = __webpack_require__("./src/app/cloud/cloud.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__scheduling_scheduling_component__ = __webpack_require__("./src/app/scheduling/scheduling.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__datasource_datasource_component__ = __webpack_require__("./src/app/datasource/datasource.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__rules_rules_component__ = __webpack_require__("./src/app/rules/rules.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__login_login_service__ = __webpack_require__("./src/app/login/login.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_primeng_primeng__ = __webpack_require__("./node_modules/primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_primeng_calendar__ = __webpack_require__("./node_modules/primeng/calendar.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_primeng_calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_primeng_calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_primeng_radiobutton__ = __webpack_require__("./node_modules/primeng/radiobutton.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_primeng_radiobutton___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_primeng_radiobutton__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_primeng_dropdown__ = __webpack_require__("./node_modules/primeng/dropdown.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_primeng_dropdown___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_primeng_dropdown__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_primeng_toast__ = __webpack_require__("./node_modules/primeng/toast.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_primeng_toast___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_primeng_toast__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_primeng_api__ = __webpack_require__("./node_modules/primeng/api.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_primeng_api___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_primeng_api__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















/* start of prime ng */






/* end of prime ng */



var XhrInterceptor = /** @class */ (function () {
    function XhrInterceptor() {
    }
    XhrInterceptor.prototype.intercept = function (req, next) {
        var xhr = req.clone({
            headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
        });
        return next.handle(xhr);
    };
    XhrInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])()
    ], XhrInterceptor);
    return XhrInterceptor;
}());

var appRoutes = [
    { path: '', redirectTo: 'cloud', pathMatch: 'full' },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_11__login_login_component__["a" /* LoginComponent */] },
    { path: 'datasource', component: __WEBPACK_IMPORTED_MODULE_9__datasource_datasource_component__["a" /* DatasourceComponent */] },
    { path: 'notification', component: __WEBPACK_IMPORTED_MODULE_6__notification_notification_component__["a" /* NotificationComponent */] },
    { path: 'cloud', component: __WEBPACK_IMPORTED_MODULE_7__cloud_cloud_component__["a" /* CloudComponent */] },
    { path: 'scheduling', component: __WEBPACK_IMPORTED_MODULE_8__scheduling_scheduling_component__["a" /* SchedulingComponent */] },
    { path: 'rules', component: __WEBPACK_IMPORTED_MODULE_10__rules_rules_component__["a" /* RulesComponent */] }
    //  { path: '**', redirectTo: 'login' } 
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__notification_notification_component__["a" /* NotificationComponent */],
                __WEBPACK_IMPORTED_MODULE_7__cloud_cloud_component__["a" /* CloudComponent */],
                __WEBPACK_IMPORTED_MODULE_8__scheduling_scheduling_component__["a" /* SchedulingComponent */],
                __WEBPACK_IMPORTED_MODULE_9__datasource_datasource_component__["a" /* DatasourceComponent */],
                __WEBPACK_IMPORTED_MODULE_10__rules_rules_component__["a" /* RulesComponent */],
                __WEBPACK_IMPORTED_MODULE_11__login_login_component__["a" /* LoginComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_15_primeng_calendar__["CalendarModule"],
                __WEBPACK_IMPORTED_MODULE_14_primeng_primeng__["InputTextModule"],
                __WEBPACK_IMPORTED_MODULE_16_primeng_radiobutton__["RadioButtonModule"],
                __WEBPACK_IMPORTED_MODULE_17_primeng_dropdown__["DropdownModule"],
                __WEBPACK_IMPORTED_MODULE_18_primeng_toast__["ToastModule"],
                __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpClientModule */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["RouterModule"].forRoot(appRoutes)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_12__login_login_service__["a" /* LoginService */], { provide: __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: XhrInterceptor, multi: true },
                __WEBPACK_IMPORTED_MODULE_20__services_configuration_service__["a" /* ConfigurationService */], __WEBPACK_IMPORTED_MODULE_21__services_common_service__["a" /* CommonService */], __WEBPACK_IMPORTED_MODULE_19_primeng_api__["MessageService"]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["RouterModule"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/cloud/cloud.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/cloud/cloud.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"title\">Megh Dooth Time & Labour Integration Tool Setup</h2>\n<div class=\"tab-nav\">\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/datasource\">Datasource</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\">Cloud</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/notification\">Notification</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/scheduling\">Scheduling</a>  \n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/rules\">Rules</a>     \n</div>\n\n<div class=\"jumbotron\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-6 offset-3\">\n        <form [formGroup]=\"cloudForm\" (ngSubmit)=\"onSubmit(cloudForm.value)\">\n          <div class=\"form-group\">\n             <label>Organization Name:</label>\n            <input  type=\"text\" pInputText class=\"form-control\" formControlName=\"organizationName\" [ngClass]=\"{ 'is-invalid': submitted && f.organizationName.errors }\">\n            <div *ngIf=\"submitted && f.organizationName.errors\" class=\"invalid-feedback\">\n              <div *ngIf=\"f.organizationName.errors.required\">Organization Name is required</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Instance URL:</label>\n            <input  type=\"text\" pInputText class=\"form-control\" formControlName=\"instanceUrl\" [ngClass]=\"{ 'is-invalid': submitted && f.instanceUrl.errors }\">\n\n            <div *ngIf=\"submitted && f.instanceUrl.errors\" class=\"invalid-feedback\">\n              <div *ngIf=\"f.instanceUrl.errors.required\">Instance URL is required</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Web Service URL:</label>\n            <input  type=\"text\" pInputText type=\"text\" class=\"form-control\" formControlName=\"webServiceUrl\" [ngClass]=\"{ 'is-invalid': submitted && f.webServiceUrl.errors }\">\n            <div *ngIf=\"submitted && f.webServiceUrl.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.webServiceUrl.errors.required\">web Service Url is required</div>\n              </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Username:</label>\n            <input  type=\"text\" pInputText class=\"form-control\" formControlName=\"userName\" [ngClass]=\"{ 'is-invalid': submitted && f.userName.errors }\">\n            <div *ngIf=\"submitted && f.webServiceUrl.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.userName.errors.required\">Username is required</div>\n              </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Password:</label>\n            <input   pInputText type=\"password\" class=\"form-control\" formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\">\n            <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.password.errors.required\">Password is required</div>\n              </div>\n          </div> \n          \n          <button type=\"proceed\" class=\"btn btn-primary fl-r\">Proceed</button> \n          <button type=\"save\" class=\"btn btn-primary fl-r m-r-20\">Save</button>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/cloud/cloud.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CloudComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CloudComponent = /** @class */ (function () {
    function CloudComponent(formBuilder, configService) {
        this.formBuilder = formBuilder;
        this.configService = configService;
        this.title = 'app';
        this.submitted = false;
        this.createForm();
    }
    CloudComponent.prototype.ngOnInit = function () {
        this.loadCloudConfig();
    };
    CloudComponent.prototype.loadCloudConfig = function () {
        var _this = this;
        this.configService.getCloudConfig(function (response) {
            if (response.status && response.data && response.data.cloud_config) {
                _this.cloudFormData = response.data.cloud_config;
                _this.buildFormControls();
                _this.cloudForm.patchValue(_this.cloudFormData);
            }
            else {
                alert("error occurred" + response.message);
            }
        });
    };
    CloudComponent.prototype.buildFormControls = function () {
        this.cloudForm = this.formBuilder.group(({
            organizationName: [this.cloudFormData.organizationName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            instanceUrl: [this.cloudFormData.instanceUrl, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            webServiceUrl: [this.cloudFormData.webServiceUrl, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            userName: [this.cloudFormData.userName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            password: [this.cloudFormData.password, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        }));
    };
    CloudComponent.prototype.createForm = function () {
        this.cloudForm = this.formBuilder.group({
            organizationName: '',
            instanceUrl: '',
            webServiceUrl: '',
            userName: '',
            password: ''
        });
    };
    Object.defineProperty(CloudComponent.prototype, "f", {
        get: function () { return this.cloudForm.controls; },
        enumerable: true,
        configurable: true
    });
    CloudComponent.prototype.onSubmit = function (valuesObj) {
        var _this = this;
        console.log("cloud Submit::::", valuesObj, this.cloudFormData);
        this.submitted = true;
        // stop here if form is invalid
        if (this.cloudForm.invalid) {
            return;
        }
        this.cloudFormData.organizationName = valuesObj.organizationName;
        this.cloudFormData.instanceUrl = valuesObj.instanceUrl;
        this.cloudFormData.webServiceUrl = valuesObj.webServiceUrl;
        this.cloudFormData.userName = valuesObj.userName;
        this.cloudFormData.password = valuesObj.password;
        this.configService.saveCloudConfig(this.cloudFormData)
            .subscribe(function (response) {
            if (response.status) {
                _this.loadCloudConfig();
                alert(response.message);
            }
            else {
                /*this.error = 'Username or password is incorrect';
                  this.loading = false; */
                alert(response.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    CloudComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-cloud',
            template: __webpack_require__("./src/app/cloud/cloud.component.html"),
            styles: [__webpack_require__("./src/app/cloud/cloud.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__["a" /* ConfigurationService */]])
    ], CloudComponent);
    return CloudComponent;
}());



/***/ }),

/***/ "./src/app/datasource/datasource.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/datasource/datasource.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"title\">Megh Dooth Time & Labour Integration Tool Setup</h2>\n<div class=\"tab-nav\">\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/datasource\">Datasource</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\">Cloud</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/notification\">Notification</a>\n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/scheduling\">Scheduling</a>  \n  <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/rules\">Rules</a>     \n</div>\n\n<div class=\"jumbotron\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-6 offset-3\">\n        <form [formGroup]=\"datasourceForm\" (ngSubmit)=\"onSubmit(datasourceForm.value)\">\n            <p-toast position=\"top-right\"></p-toast>\n          <div class=\"form-group\">\n             <label>Datasource Type:</label>\n            <p-dropdown [options]=\"datasources\" formControlName=\"datasourceType\" [ngClass]=\"{ 'is-invalid': submitted && f.datasourceType.errors }\"></p-dropdown>\n            <div *ngIf=\"submitted && f.datasourceType.errors\" class=\"invalid-feedback\">\n              <div *ngIf=\"f.datasourceType.errors.required\">Datasource Type is required</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Host Name:</label>\n            <input  type=\"text\" pInputText class=\"form-control\" formControlName=\"hostName\" [ngClass]=\"{ 'is-invalid': submitted && f.hostName.errors }\">\n            <!-- <div *ngIf=\"submitted && f.hostName.errors\" class=\"invalid-feedback\">\n              <div *ngIf=\"f.hostName.errors.required\">Host Name is required</div>\n            </div> -->\n          </div>\n          <div class=\"form-group\">\n            <label>Port:</label>\n            <input  type=\"text\" pInputText type=\"text\" class=\"form-control\" formControlName=\"port\" [ngClass]=\"{ 'is-invalid': submitted && f.port.errors }\">\n            <div *ngIf=\"submitted && f.port.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.port.errors.required\">Port is required</div>\n              </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Schema Name:</label>\n            <input  type=\"text\" pInputText type=\"text\" class=\"form-control\" formControlName=\"schemaName\" [ngClass]=\"{ 'is-invalid': submitted && f.schemaName.errors }\">\n            <div *ngIf=\"submitted && f.schemaName.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.schemaName.errors.required\">Schema Name is required</div>\n              </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Username:</label>\n            <input  type=\"text\" pInputText class=\"form-control\" formControlName=\"userName\" [ngClass]=\"{ 'is-invalid': submitted && f.userName.errors }\">\n            <div *ngIf=\"submitted && f.userName.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.userName.errors.required\">Username is required</div>\n              </div>\n          </div>\n          <div class=\"form-group\">\n            <label>Password:</label>\n            <input   pInputText type=\"password\" class=\"form-control\" formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\">\n            <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.password.errors.required\">Password is required</div>\n              </div>\n          </div> \n          \n          <button type=\"proceed\" class=\"btn btn-primary fl-r\">Proceed</button> \n          <button type=\"save\" class=\"btn btn-primary fl-r m-r-20\">Save</button>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/datasource/datasource.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatasourceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_api__ = __webpack_require__("./node_modules/primeng/api.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_primeng_api___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_primeng_api__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DatasourceComponent = /** @class */ (function () {
    function DatasourceComponent(formBuilder, configService, messageService) {
        this.formBuilder = formBuilder;
        this.configService = configService;
        this.messageService = messageService;
        this.title = 'app';
        this.submitted = false;
        this.createForm();
    }
    DatasourceComponent.prototype.ngOnInit = function () {
        this.datasources = [
            { label: '-- Select --', value: null },
            { label: 'My SQL', value: 'mysql' },
            { label: 'MS SQL', value: 'MS SQL' },
            { label: 'Postgre SQL', value: 'Postgre SQL' },
            { label: 'Oracle', value: 'Oracle' }
        ];
        this.loadDatsource();
    };
    DatasourceComponent.prototype.loadDatsource = function () {
        var _this = this;
        this.configService.getDatasource(function (response) {
            if (response.status && response.data && response.data.datasource) {
                _this.datasourceFormData = response.data.datasource;
                _this.buildFormControls();
                _this.datasourceForm.patchValue(_this.datasourceFormData);
            }
            else {
                alert("error occurred" + response.message);
            }
        });
    };
    DatasourceComponent.prototype.buildFormControls = function () {
        this.datasourceForm = this.formBuilder.group(({
            datasourceType: [this.datasourceFormData.datasourceType, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            hostName: [this.datasourceFormData.hostName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            port: [this.datasourceFormData.port, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            schemaName: [this.datasourceFormData.schemaName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            userName: [this.datasourceFormData.userName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            password: [this.datasourceFormData.password, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        }));
    };
    DatasourceComponent.prototype.createForm = function () {
        this.datasourceForm = this.formBuilder.group({
            datasourceType: '',
            hostName: '',
            port: '',
            schemaName: '',
            userName: '',
            password: ''
        });
    };
    Object.defineProperty(DatasourceComponent.prototype, "f", {
        get: function () { return this.datasourceForm.controls; },
        enumerable: true,
        configurable: true
    });
    DatasourceComponent.prototype.onSubmit = function (valuesObj) {
        var _this = this;
        console.log("datasource Submit::::", valuesObj, this.datasourceFormData);
        this.submitted = true;
        // stop here if form is invalid
        if (this.datasourceForm.invalid) {
            return;
        }
        this.datasourceFormData.datasourceType = valuesObj.datasourceType;
        this.datasourceFormData.hostName = valuesObj.hostName;
        this.datasourceFormData.port = valuesObj.port;
        this.datasourceFormData.schemaName = valuesObj.schemaName;
        this.datasourceFormData.userName = valuesObj.userName;
        this.datasourceFormData.password = valuesObj.password;
        this.configService.saveDatasource(this.datasourceFormData)
            .subscribe(function (response) {
            if (response.status) {
                _this.loadDatsource();
                _this.messageService.add({ severity: 'success', summary: 'Datasource Configuration Saved Sucessfully' });
            }
            else {
                /*this.error = 'Username or password is incorrect';
                  this.loading = false; */
                _this.messageService.add({ severity: 'error', summary: 'Error occurred in Datasource Configuration' });
            }
        }, function (error) {
            console.log(error);
        });
    };
    DatasourceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-cloud',
            template: __webpack_require__("./src/app/datasource/datasource.component.html"),
            styles: [__webpack_require__("./src/app/datasource/datasource.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__["a" /* ConfigurationService */], __WEBPACK_IMPORTED_MODULE_3_primeng_api__["MessageService"]])
    ], DatasourceComponent);
    return DatasourceComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".container{ border-radius: 10px;}\r\n.forgot-link { padding: 0;}\r\n.fl-r{float:right;}\r\n"

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"col-md-12  home-container\">\n\t<div class=\"row\">\n\t\t<div class=\"col-md-5 login-img min-h-400\">\n\t\t\t\t<img src=\"assets/images/sierra-cedar_logo.png\" alt=\"\" class=\"logo\">\n\t\t</div>\n\t<div class=\"col-md-7 min-h-400 pt-50\">\n\t<div class=\"alert alert-danger\" [hidden]=\"!error\">\n\t\tThere was a problem logging in. Please try again.\n\t</div>\n\n\t<form role=\"form\" (submit)=\"login()\">\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"username\">Username</label>\n\t\t\t<input pInputText type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" [(ngModel)]=\"credentials.username\"\n\t\t\t/>\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"password\">Password</label>\n\t\t\t<input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"credentials.password\"\n\t\t\t/>\n\t\t</div>\n\t\t<a routerLinkActive=\"active\" class=\"nav-link forgot-link\" routerLink=\"\" (click)=\"logout()\">Forgot password?</a>\n\t\t<div><button type=\"submit\" class=\"btn btn-primary fl-r\">Login</button></div>\n\t</form>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_service__ = __webpack_require__("./src/app/login/login.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(app, http, router) {
        this.app = app;
        this.http = http;
        this.router = router;
        this.credentials = { username: '', password: '' };
        this.loading = false;
        this.error = '';
    }
    LoginComponent.prototype.login = function () {
        /*  this.app.authenticate(this.credentials, () => {
           if(this.app.authenticate) {
             this.router.navigateByUrl('/');
           } else {
             this.router.navigateByUrl('/login');;
           }
         }); */
        var _this = this;
        this.app.authenticate(this.credentials)
            .subscribe(function (result) {
            if (result === true) {
                _this.router.navigateByUrl('/');
            }
            else {
                // login failed
                _this.error = 'Username or password is incorrect';
                _this.loading = false;
            }
        }, function (error) {
            _this.loading = false;
            _this.error = error;
        });
        /*  this.app.authenticate(this.credentials);
         if(this.app.authenticate)
           this.router.navigate(['/cloud']);
         else
         this.router.navigate(['/']);
         return true; */
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("./src/app/login/login.component.html"),
            styles: [__webpack_require__("./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__login_service__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/login.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
        this.authenticated = false;
        this.organizationId = '';
        this.organizationId = localStorage.getItem('organizationId');
    }
    /* authenticate(credentials, callback) {
      const headers = new HttpHeaders(credentials ? {
          authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password)
      } : {});
  
      this.http.get('setup/authenticate', {headers: headers}).subscribe(response => {
          if (response['name']) {
              this.authenticated = true;
          } else {
              this.authenticated = false;
          }
          return callback && callback();
      });
   }, */
    LoginService.prototype.isUserLoogedIn = function (callback) {
        var _this = this;
        return this.http.get('setup/getUser').subscribe(function (response) {
            var userId = localStorage.getItem("userId");
            if (response.data != null && response.data.user && userId == response.data.user.userId) {
                _this.authenticated = true;
            }
            else {
                _this.authenticated = false;
            }
            return callback && callback(_this.authenticated);
        });
    };
    LoginService.prototype.authenticate = function (credentials) {
        var _this = this;
        var myheader = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]().set('Content-Type', 'application/x-www-form-urlencoded');
        var body = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["e" /* HttpParams */]();
        body = body.set('username', credentials.username);
        body = body.set('password', credentials.password);
        return this.http.post('/setup/authenticate', body, { headers: myheader }) //.subscribe(); 
            .map(function (response) {
            // login successful if there's a jwt token in the response 
            if (response.status) {
                _this.organizationId = response.data.user.organization.id + "";
                localStorage.setItem('currentUser', JSON.stringify(response.data.user));
                localStorage.setItem('userId', response.data.user.id);
                localStorage.setItem('organizationId', response.data.user.organization.id + "");
                // return true to indicate successful login
                _this.authenticated = true;
                return true;
            }
            else {
                // return false to indicate failed login
                _this.authenticated = false;
                localStorage.removeItem('userId');
                localStorage.removeItem('currentUser');
                localStorage.removeItem('organizationId');
                _this.organizationId = '';
                return false;
            }
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error.json().error || 'Server error'); });
    };
    LoginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/notification/notification.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/notification/notification.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"title\">Megh Dooth Time & Labour Integration Tool Setup</h2>\r\n<div class=\"tab-nav\">\r\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/datasource\">Datasource</a>\r\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\">Cloud</a>\r\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/notification\">Notification</a>\r\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/scheduling\">Scheduling</a>  \r\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/rules\">Rules</a>  \r\n</div>\r\n<div class=\"jumbotron\"> \r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-6 offset-md-3\">\r\n                <form [formGroup]=\"notificationForm\" (ngSubmit)=\"onSubmit(notificationForm.value)\">\r\n                    <div class=\"form-group\">\r\n                        <input type=\"checkbox\" formControlName=\"emailNotificationRequired\" [(ngModel)]=\"emailNotificationRequired\" />\r\n                        <label>Email Notification Required : </label>\r\n                    </div>\r\n                    <div>\r\n                        <div class=\"form-group\">\r\n                            <label>Server/Host::</label>\r\n                            <input type=\"text\" class=\"form-control\" formControlName=\"hostName\" [ngClass]=\"{ 'is-invalid': submitted && f.hostName.errors }\">\r\n                            <div *ngIf=\"submitted && myControl.hostName.errors\" class=\"invalid-feedback\">\r\n                                <div *ngIf=\"f.hostName.errors.required\">hostName is required</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label>Port :</label>\r\n                            <input type=\"text\" class=\"form-control\" formControlName=\"port\" [ngClass]=\"{ 'is-invalid': submitted && f.port.errors }\">\r\n                            <div *ngIf=\"submitted && f.port.errors\" class=\"invalid-feedback\">\r\n                                <div *ngIf=\"f.port.errors.required\">port is required</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label>Username:</label>\r\n                        <input type=\"text\" class=\"form-control\" formControlName=\"userName\" [ngClass]=\"{ 'is-invalid': submitted && f.userName.errors }\">\r\n                        <div *ngIf=\"submitted && f.userName.errors\" class=\"invalid-feedback\">\r\n                            <div *ngIf=\"f.userName.errors.required\">Username is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label>Password:</label>\r\n                        <input type=\"password\" class=\"form-control\" formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\">\r\n                        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n                            <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label>from:</label>\r\n                        <input type=\"text\" class=\"form-control\" formControlName=\"fromEmailId\" [ngClass]=\"{ 'is-invalid': submitted && f.fromEmailId.errors }\">\r\n                        <div *ngIf=\"submitted && f.fromEmailId.errors\" class=\"invalid-feedback\">\r\n                            <div *ngIf=\"f.fromEmailId.errors.required\">from is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label>subject :</label>\r\n                        <input type=\"text\" class=\"form-control\" formControlName=\"subject\" [ngClass]=\"{ 'is-invalid': submitted && f.subject.errors }\">\r\n                        <div *ngIf=\"submitted && f.subject.errors\" class=\"invalid-feedback\">\r\n                            <div *ngIf=\"f.subject.errors.required\">subject is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <input type=\"checkbox\" formControlName=\"dailyNotification\" />\r\n                        <label>Daily Notification </label>\r\n                        <input type=\"checkbox\" formControlName=\"onErrorNotification\" />\r\n                        <label>On Error Notification </label>\r\n                    </div>\r\n                     <button type=\"proceed\" class=\"btn btn-primary fl-r\">Proceed</button> \r\n                    <button type=\"save\" class=\"btn btn-primary fl-r m-r-20\">Save</button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/notification/notification.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotificationComponent = /** @class */ (function () {
    function NotificationComponent(formBuilder, configService) {
        this.formBuilder = formBuilder;
        this.configService = configService;
        this.submitted = false;
        this.createForm();
    }
    NotificationComponent.prototype.ngOnInit = function () {
        this.loadNotificationConfig();
    };
    NotificationComponent.prototype.loadNotificationConfig = function () {
        var _this = this;
        this.configService.getNotificationConfig(function (response) {
            if (response.status && response.data && response.data.notification_config) {
                _this.notificationFormData = response.data.notification_config;
                _this.buildFormControls();
                _this.notificationForm.patchValue(_this.notificationFormData);
            }
            else {
                alert("error occurred" + response.message);
            }
        });
    };
    NotificationComponent.prototype.buildFormControls = function () {
        this.notificationForm = this.formBuilder.group(({
            emailNotificationRequired: [this.notificationFormData.emailNotificationRequired],
            hostName: [this.notificationFormData.hostName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            port: [this.notificationFormData.port, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            userName: [this.notificationFormData.userName, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            password: [this.notificationFormData.password, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            fromEmailId: [this.notificationFormData.fromEmailId, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            subject: [this.notificationFormData.fromEmailId, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            dailyNotification: [this.notificationFormData.fromEmailId],
            onErrorNotification: [this.notificationFormData.onErrorNotification]
        }));
    };
    NotificationComponent.prototype.createForm = function () {
        this.notificationForm = this.formBuilder.group({
            emailNotificationRequired: [''],
            hostName: [''],
            port: [''],
            userName: [''],
            password: [''],
            fromEmailId: [''],
            subject: [''],
            dailyNotification: [''],
            onErrorNotification: ['']
        });
    };
    Object.defineProperty(NotificationComponent.prototype, "f", {
        get: function () {
            return this.notificationForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    NotificationComponent.prototype.onSubmit = function (valuesObj) {
        var _this = this;
        console.log("Notification Submit::::", valuesObj, this.notificationFormData);
        this.submitted = true;
        // stop here if form is invalid
        if (this.notificationForm.invalid) {
            return;
        }
        this.setFields(valuesObj);
        this.configService.saveNotificationConfig(this.notificationFormData)
            .subscribe(function (response) {
            if (response.status) {
                _this.loadNotificationConfig();
                alert(response.message);
            }
            else {
                /*this.error = 'Username or password is incorrect';
                  this.loading = false; */
                alert(response.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    NotificationComponent.prototype.setFields = function (valuesObj) {
        this.notificationFormData.emailNotificationRequired = valuesObj.emailNotificationRequired;
        this.notificationFormData.hostName = valuesObj.hostName;
        this.notificationFormData.port = valuesObj.port;
        this.notificationFormData.userName = valuesObj.userName;
        this.notificationFormData.password = valuesObj.password;
        this.notificationFormData.fromEmailId = valuesObj.fromEmailId;
        this.notificationFormData.subject = valuesObj.subject;
        this.notificationFormData.dailyNotification = valuesObj.dailyNotification;
        this.notificationFormData.onErrorNotification = valuesObj.onErrorNotification;
    };
    NotificationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__("./src/app/notification/notification.component.html"),
            styles: [__webpack_require__("./src/app/notification/notification.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__["a" /* ConfigurationService */]])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/rules/rules.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/rules/rules.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"title\">Megh Dooth Time & Labour Integration Tool Setup</h2>\n<div class=\"tab-nav\">\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/datasource\">Datasource</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\">Cloud</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/notification\">Notification</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/scheduling\">Scheduling</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/rules\">Rules</a>\n    <!-- <a href=\"./cloud\" > Cloud</a>\n<a href=\"./notification\">Notification</a>\n<a href=\"./scheduling\">Scheduling</a>\n<a href=\"./rules\" class=\"active\">Rules</a> -->\n</div>\n\n<div class=\"jumbotron\">\n    <div class=\"container\">\n        <div class=\"row\">\n\n            <div class=\"col-md-6 offset-md-3\">\n                <form [formGroup]=\"rulesForm\" (ngSubmit)=\"onSubmit(rulesForm.value)\">\n                    <div class=\"form-group\">\n                        <input type=\"checkbox\" formControlName=\"removeDuplicates\" [(ngModel)]=\"removeDuplicates\" />\n                        <label>Remove Duplicates : </label>\n                    </div>\n                    <div class=\"form-group\">\n                        <label>Shift Prioritization: </label>\n                        <select name=\"shifPriority\" formControlName=\"shifPriority\" class=\"form-control\"\n                            [ngClass]=\"{ 'is-invalid': submitted && f.shifPriority.errors }\"\n                        > \n                            <option value=\"\" disabled selected>Select your option</option>\n                            <option [value]=\"freq\" *ngFor=\"let freq of shiftPrioritizationData\">\n                                {{freq}}</option>\n                        </select>  \n                        <div *ngIf=\"submitted && f.shifPriority.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.shifPriority.errors.required\">Shift Prioritization is required</div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <label>Shift Difference: </label>\n                        <input formControlName=\"shiftDifferences\" class=\"form-control\" maxlength=\"23\" \n                         size=\"2\" min=\"0\" max=\"24\" type=\"number\"\n                          [ngClass]=\"{ 'is-invalid': submitted && f.shiftDifferences.errors }\"\n                        /> Hour(s)\n                        <div *ngIf=\"submitted && f.shiftDifferences.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.shiftDifferences.errors.required\">Shift Difference is required</div>\n                        </div>\n                    </div>\n\n                    <button type=\"proceed\" class=\"btn btn-primary fl-r\">Proceed</button> \n                    <button type=\"save\" class=\"btn btn-primary fl-r m-r-20\">Save</button>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/rules/rules.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RulesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RulesComponent = /** @class */ (function () {
    function RulesComponent(formBuilder, configService) {
        this.formBuilder = formBuilder;
        this.configService = configService;
        this.submitted = false;
        this.shiftPrioritizationData = [
            'FIFO',
            'FILO',
            'LIFO',
            'LILO'
        ];
        this.createForm();
    }
    RulesComponent.prototype.createForm = function () {
        this.rulesForm = this.formBuilder.group({
            removeDuplicates: [false],
            shifPriority: [''],
            shiftDifferences: ['']
        });
    };
    RulesComponent.prototype.ngOnInit = function () {
        this.loadRulesConfig();
    };
    RulesComponent.prototype.loadRulesConfig = function () {
        var _this = this;
        this.configService.getRulesConfig(function (response) {
            if (response.status && response.data && response.data.rules_config) {
                _this.rulesFormData = response.data.rules_config;
                _this.buildFormControls();
                _this.rulesForm.patchValue(response.data.rules_config);
            }
            else {
                alert("error occurred" + response.message);
            }
        });
    };
    RulesComponent.prototype.buildFormControls = function () {
        this.rulesForm = this.formBuilder.group(({
            removeDuplicates: [this.rulesFormData.removeDuplicates],
            shifPriority: [this.rulesFormData.shifPriority, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            shiftDifferences: [this.rulesFormData.shiftDifferences, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]
        }));
    };
    Object.defineProperty(RulesComponent.prototype, "f", {
        get: function () {
            return this.rulesForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    RulesComponent.prototype.onSubmit = function (valuesObj) {
        var _this = this;
        console.log("Ruls Submit::::", valuesObj, this.rulesFormData);
        this.submitted = true;
        // stop here if form is invalid
        if (this.rulesForm.invalid) {
            return;
        }
        this.setFields(valuesObj);
        // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.rulesForm.value))
        this.configService.saveRulesConfig(this.rulesFormData)
            .subscribe(function (response) {
            if (response.status) {
                _this.loadRulesConfig();
                alert(response.message);
            }
            else {
                /*this.error = 'Username or password is incorrect';
                  this.loading = false; */
                alert(response.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    RulesComponent.prototype.setFields = function (valuesObj) {
        this.rulesFormData.removeDuplicates = valuesObj.removeDuplicates;
        this.rulesFormData.shifPriority = valuesObj.shifPriority;
        this.rulesFormData.shiftDifferences = valuesObj.shiftDifferences;
    };
    RulesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-rules',
            template: __webpack_require__("./src/app/rules/rules.component.html"),
            styles: [__webpack_require__("./src/app/rules/rules.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__["a" /* ConfigurationService */]])
    ], RulesComponent);
    return RulesComponent;
}());



/***/ }),

/***/ "./src/app/scheduling/scheduling.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/scheduling/scheduling.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"title\">Megh Dooth Time & Labour Integration Tool Setup</h2>\n<div class=\"tab-nav\">\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/datasource\">Datasource</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/cloud\">Cloud</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/notification\">Notification</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/scheduling\">Scheduling</a>\n    <a routerLinkActive=\"active\" class=\"nav-link\" routerLink=\"/rules\">Rules</a>\n</div>\n\n<div class=\"jumbotron\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-6 offset-md-3\">\n                <form [formGroup]=\"schedulingForm\" (ngSubmit)=\"onSubmit(schedulingForm.value)\">\n                    <div class=\"form-group\">\n                        <label>File Path:</label>\n                        <input type=\"text\" class=\"form-control\" formControlName=\"filePath\" [ngClass]=\"{ 'is-invalid': submitted && f.filePath.errors }\">\n                        <div *ngIf=\"submitted && f.filePath.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.filePath.errors.required\">File Path is required</div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <label>Frequency: </label>\n                        <select name=\"frequencyType\" formControlName=\"frequencyType\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.frequencyType.errors }\">\n                            <option value=\"\" selected>Select your option</option>\n                            <option [value]=\"freq\" *ngFor=\"let freq of frequencyData\">\n                                {{freq}}</option>\n                        </select>\n                        <div *ngIf=\"submitted && f.frequencyType.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.frequencyType.errors.required\">Frequency is required</div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <div *ngIf=\"schedulingForm.value.frequencyType === 'Minutes/Hour'\" class=\"ui-g\" style=\"width:250px;margin-bottom:10px\">\n                            <div class=\"ui-g-12\">\n                                <p-radioButton name=\"everyInterval\" formControlName=\"everyInterval\" value=\"Hour\" inputId=\"hourOpt1\"></p-radioButton>\n                                <input formControlName=\"hourInterval\" class=\"form-control\" maxlength=\"23\" size=\"2\" min=\"1\" max=\"24\" type=\"number\" [ngClass]=\"{ 'is-invalid':submitted && f.hourInterval.errors\n                                 && (schedulingForm.value.everyInterval === 'Hour') }\" /> Hour(s)\n                                <div *ngIf=\"submitted && f.hourInterval.errors\n                                && (schedulingForm.value.everyInterval === 'Hour')\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.hourInterval.errors.required\">Hours is required</div>\n                                </div>\n                            </div>\n                            <div class=\"ui-g-12\">\n                                <p-radioButton name=\"everyInterval\" formControlName=\"everyInterval\" value=\"Minute\" inputId=\"minuteOpt2\"></p-radioButton>\n                                <input formControlName=\"minuteInterval\" class=\"form-control\" maxlength=\"23\" size=\"2\" min=\"1\" max=\"60\" type=\"number\" [ngClass]=\"{ 'is-invalid':submitted && f.minuteInterval.errors\n                                 && (schedulingForm.value.everyInterval === 'Minute') }\" /> Minute(s)\n                                <div *ngIf=\"submitted && f.minuteInterval.errors\n                                && (schedulingForm.value.everyInterval === 'Minute')\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.minuteInterval.errors.required\">Minutes is required</div>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"schedulingForm.value.frequencyType === 'Monthly'\">\n                            <select name=\"monthlyDay\" formControlName=\"monthlyDay\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.monthlyDay.errors }\">\n                                <option value=\"\" disabled selected>Select your option</option>\n                                <option value=\"First Day of the Month\">\n                                    First Day of the Month\n                                </option>\n                                <option value=\"Last Day of the Month\">\n                                    Last Day of the Month\n                                </option>\n                            </select>\n                            <div *ngIf=\"submitted && f.monthlyDay.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.monthlyDay.errors.required\">Frequency is required</div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"schedulingForm.value.frequencyType === 'Weekly OR Bi-Weekly'\">\n\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <label>Start Date Time: </label>\n                        <!--  <input type=\"datetime-local\" class=\"form-control\" \n                        formControlName=\"startTime\" \n                         [ngModel]=\"filteStartDateFrom | date:'yyyy-MM-ddTHH:mm'\"\n                            (ngModelChange)=\"startTime = $event.value\"> -->\n                        <p-calendar showIcon=\"true\" formControlName=\"startTime\" showTime=\"true\" hourFormat=\"12\"></p-calendar>\n                        <div *ngIf=\"submitted && f.startTime.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.startTime.errors.required\">Start Date Time is required</div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <label>End Date Time: </label>\n                        <!-- <input type=\"datetime-local\" class=\"form-control\"\n                             formControlName=\"endTime\" [ngModel]=\"filterendTimeFrom | \n                             date:'yyyy-MM-ddTHH:mm'\"\n                            (ngModelChange)=\"endTime = $event.target.value\">   -->\n                        <p-calendar showIcon=\"true\" formControlName=\"endTime\" showTime=\"true\" hourFormat=\"24\"></p-calendar>\n                        <div *ngIf=\"submitted && f.endTime.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.endTime.errors.required\">End Date Time is required</div>\n                        </div>\n\n                    </div>\n                    <button type=\"proceed\" class=\"btn btn-primary fl-r\">Proceed</button>\n                    <button type=\"save\" class=\"btn btn-primary fl-r m-r-20\">Save</button>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/scheduling/scheduling.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SchedulingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__ = __webpack_require__("./src/app/services/configuration.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_common_service__ = __webpack_require__("./src/app/services/common.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SchedulingComponent = /** @class */ (function () {
    function SchedulingComponent(formBuilder, configService, commonService) {
        this.formBuilder = formBuilder;
        this.configService = configService;
        this.commonService = commonService;
        this.submitted = false;
        this.frequencyData = [
            'Once',
            'Minutes/Hour',
            'Daily',
            'Weekly OR Bi-Weekly',
            'Monthly'
        ];
        this.createForm();
    }
    SchedulingComponent.prototype.createForm = function () {
        this.schedulingForm = this.formBuilder.group({
            filePath: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            frequencyType: ['Once', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            startTime: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            endTime: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            everyInterval: [''],
            hourInterval: [''],
            minuteInterval: [''],
            monthlyDay: ['']
        });
    };
    SchedulingComponent.prototype.ngOnInit = function () {
        this.loadSchedulingConfig();
    };
    SchedulingComponent.prototype.loadSchedulingConfig = function () {
        var _this = this;
        this.configService.getSchedulingConfig(function (response) {
            if (response.status && response.data && response.data.schedule_config) {
                _this.schedulingFormData = response.data.schedule_config;
                if (_this.schedulingFormData.id != null) {
                    _this.buildFormControls();
                    _this.schedulingForm.patchValue(_this.schedulingFormData);
                }
            }
            else {
                alert("error occurred" + response.message);
            }
        });
    };
    SchedulingComponent.prototype.buildFormControls = function () {
        this.schedulingFormData.startTime = this.getDate(this.schedulingFormData.startTime);
        this.schedulingFormData.endTime = this.getDate(this.schedulingFormData.endTime);
        this.schedulingFormData.monthlyDay = this.commonService.formatString(this.schedulingFormData.monthlyDay);
        this.schedulingForm = this.formBuilder.group(({
            filePath: [this.schedulingFormData.filePath, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            frequencyType: [this.schedulingFormData.frequencyType, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            startTime: [this.schedulingFormData.startTime, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            endTime: [this.schedulingFormData.endTime, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            everyInterval: [this.schedulingFormData.everyInterval],
            hourInterval: [this.schedulingFormData.hourInterval],
            minuteInterval: [this.schedulingFormData.minuteInterval],
            monthlyDay: [this.schedulingFormData.monthlyDay]
        }));
    };
    SchedulingComponent.prototype.getDate = function (date) {
        if (date != null && date != "") {
            return new Date(date);
        }
        return "";
    };
    Object.defineProperty(SchedulingComponent.prototype, "f", {
        get: function () {
            return this.schedulingForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    SchedulingComponent.prototype.onSubmit = function (valuesObj) {
        var _this = this;
        console.log("scheduling Submit::::", valuesObj, this.schedulingFormData);
        this.submitted = true;
        // stop here if form is invalid
        if (this.schedulingForm.invalid) {
            this.findInvalidControls();
            return;
        }
        this.setFields(valuesObj);
        this.configService.saveSchedulingConfig(this.schedulingFormData)
            .subscribe(function (response) {
            if (response.status) {
                _this.loadSchedulingConfig();
                alert(response.message);
            }
            else {
                /*this.error = 'Username or password is incorrect';
                  this.loading = false; */
                alert(response.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    SchedulingComponent.prototype.findInvalidControls = function () {
        var invalid = [];
        var controls = this.schedulingForm.controls;
        for (var name_1 in controls) {
            if (controls[name_1].invalid) {
                invalid.push(name_1);
            }
        }
        console.log("invalids are:::" + invalid);
        return invalid;
    };
    SchedulingComponent.prototype.setFields = function (valuesObj) {
        this.schedulingFormData.filePath = valuesObj.filePath;
        this.schedulingFormData.frequencyType = valuesObj.frequencyType;
        this.schedulingFormData.startTime = valuesObj.startTime;
        this.schedulingFormData.endTime = valuesObj.endTime;
        this.schedulingFormData.everyInterval = valuesObj.everyInterval;
        this.schedulingFormData.hourInterval = valuesObj.hourInterval;
        this.schedulingFormData.minuteInterval = valuesObj.minuteInterval;
        this.schedulingFormData.monthlyDay = valuesObj.monthlyDay;
    };
    SchedulingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-scheduling',
            template: __webpack_require__("./src/app/scheduling/scheduling.component.html"),
            styles: [__webpack_require__("./src/app/scheduling/scheduling.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_2__services_configuration_service__["a" /* ConfigurationService */],
            __WEBPACK_IMPORTED_MODULE_3__services_common_service__["a" /* CommonService */]])
    ], SchedulingComponent);
    return SchedulingComponent;
}());



/***/ }),

/***/ "./src/app/services/common.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CommonService = /** @class */ (function () {
    function CommonService(http) {
        this.http = http;
    }
    CommonService.prototype.formatString = function (str) {
        if (str == null || str == "" || str == undefined) {
            return "";
        }
        return str;
    };
    CommonService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], CommonService);
    return CommonService;
}());



/***/ }),

/***/ "./src/app/services/configuration.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigurationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login_service__ = __webpack_require__("./src/app/login/login.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ConfigurationService = /** @class */ (function () {
    function ConfigurationService(http, loginService) {
        this.http = http;
        this.loginService = loginService;
        this.authenticated = false;
        this.myHeaders = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({ 'organizationId': this.loginService.organizationId,
            'Accept': 'application/json', 'Content-Type': 'application/json' });
    }
    ConfigurationService.prototype.getCloudConfig = function (callback) {
        return this.http.get('configuration/getCloudConfigByOrganization?id=' + this.loginService.organizationId, { headers: this.myHeaders })
            .subscribe(function (response) {
            if (response.data != null) {
            }
            else {
            }
            return callback && callback(response);
        });
    };
    ConfigurationService.prototype.saveCloudConfig = function (config) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]();
        headers = headers.append('organizationId', this.loginService.organizationId);
        headers = headers.append('Accept', 'application/json');
        headers = headers.append('Content-Type', 'application/json');
        config['organization'] = {};
        config['organization'].id = this.loginService.organizationId;
        return this.http.post("/configuration/saveCloudConfig", config, { headers: headers })
            .map(function (response) {
            console.log("cloud saving response:::::" + response);
            return response;
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.saveNotificationConfig = function (config) {
        config['organization'] = {};
        config['organization'].id = this.loginService.organizationId;
        return this.http.post("/configuration/saveNotificationConfig", config, { headers: this.myHeaders })
            .map(function (response) {
            console.log("Notification saving response:::::" + response);
            return response;
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.getNotificationConfig = function (callback) {
        return this.http.get('configuration/getNotificationConfigByOrganization?id=' + this.loginService.organizationId, { headers: this.myHeaders })
            .subscribe(function (response) {
            if (response.data != null) {
            }
            else {
            }
            return callback && callback(response);
        });
    };
    ConfigurationService.prototype.saveSchedulingConfig = function (config) {
        config['organization'] = {};
        config['organization'].id = this.loginService.organizationId;
        return this.http.post("/configuration/savaScheduleConfig", config, { headers: this.myHeaders })
            .map(function (response) {
            console.log("Scheduling saving response:::::" + response);
            return response;
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.getSchedulingConfig = function (callback) {
        return this.http.get('configuration/getScheduleConfigByOrganization?id=' + this.loginService.organizationId, { headers: this.myHeaders })
            .subscribe(function (response) {
            if (response.data != null) {
            }
            else {
            }
            return callback && callback(response);
        });
    };
    ConfigurationService.prototype.saveRulesConfig = function (config) {
        config['organization'] = {};
        config['organization'].id = this.loginService.organizationId;
        return this.http.post("/configuration/saveRulesConfig", config, { headers: this.myHeaders })
            .map(function (response) {
            console.log("Rules saving response:::::" + response);
            return response;
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.getRulesConfig = function (callback) {
        return this.http.get('configuration/getRulesConfigByOrganization?id=' + this.loginService.organizationId, { headers: this.myHeaders })
            .subscribe(function (response) {
            if (response.data != null) {
            }
            else {
            }
            return callback && callback(response);
        });
    };
    ConfigurationService.prototype.saveConfiguration = function (config, url) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('organizationId', this.loginService.organizationId);
        return this.http.post(url, config, { headers: headers })
            .map(function (response) {
            console.log("response:::::" + response);
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.saveDatasource = function (config) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]();
        headers = headers.append('organizationId', this.loginService.organizationId);
        headers = headers.append('Accept', 'application/json');
        headers = headers.append('Content-Type', 'application/json');
        config['organization'] = {};
        config['organization'].id = this.loginService.organizationId;
        return this.http.post("/configuration/saveDatasource", config, { headers: headers })
            .map(function (response) {
            console.log("datsource saving response:::::" + response);
            return response;
        }).catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error || 'Server error'); });
    };
    ConfigurationService.prototype.getDatasource = function (callback) {
        return this.http.get('configuration/getDatasourceByOrganization?id=' + this.loginService.organizationId, { headers: this.myHeaders })
            .subscribe(function (response) {
            if (response.data != null) {
            }
            else {
            }
            return callback && callback(response);
        });
    };
    ConfigurationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__login_login_service__["a" /* LoginService */]])
    ], ConfigurationService);
    return ConfigurationService;
}());



/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills__ = __webpack_require__("./src/polyfills.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");



Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ "./src/polyfills.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_reflect__ = __webpack_require__("./node_modules/core-js/es6/reflect.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es7_reflect__ = __webpack_require__("./node_modules/core-js/es7/reflect.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_zone_js_dist_zone__ = __webpack_require__("./node_modules/zone.js/dist/zone.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_zone_js_dist_zone__);





/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map